﻿using System.Xml;

namespace Saturn.Core.API
{
    public interface IXmlService
    {
        XmlDocument GetXmlDocument(string xmlAsString);

        string GetValue(XmlDocument xmlDocument, string xPathExpression);
    }
}
