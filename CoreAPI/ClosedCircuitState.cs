﻿using System;

namespace Saturn.Core.API
{
    public class ClosedCircuitState : ICircuitState
    {
        private readonly TimeSpan timeout;
        private bool tripped;

        public ClosedCircuitState(TimeSpan timeout)
        {
            this.timeout = timeout;
        }


        public void Guard()
        {
        }

        public ICircuitState NextState()
        {
            if (this.tripped)
            {
                return new OpenCircuitState(this.timeout);
            }
            return this;
        }

        public void Succeed()
        {
        }

        public void Trip(Exception e)
        {
            this.tripped = true;
        }

    }
}
