﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Core.API
{
    [Serializable]
    [DataContract(Name = "HttpResult", Namespace = "http://schemas.elavon.com.br")]
    public class HttpResult
    {
        [DataMember]
        public string Request { get; set; }
        [DataMember]
        public string Response { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public DateTime EndTime { get; set; }
    }
}
