﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Saturn.Core.API
{
    public interface ICircuitBreaker
    {
        void Guard();

        void Trip(Exception e);

        void Succeed();
    }
}
