﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;

namespace Saturn.Core.API
{
    public class ServiceInstaller : IWindsorInstaller
    {

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes
                    .FromAssemblyNamed("CoreAPI")
                    .Where(t => t.Name.EndsWith("Service"))
                    .WithServiceAllInterfaces()
            );
        }
    }
}
