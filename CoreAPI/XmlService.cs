﻿using log4net;
using System.Xml;

namespace Saturn.Core.API
{
    public class XmlService : IXmlService
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public XmlService()
        {
            log.Debug("Creating instance...");
        }

        public XmlDocument GetXmlDocument(string xmlAsString)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlAsString);
            return xmlDocument;
        }

        public string GetValue(XmlDocument xmlDocument, string xPathExpression)
        {
            string value = "";

            XmlNode xmlNode = xmlDocument.SelectSingleNode(xPathExpression);
            if (xmlNode != null)
            {
                if (xmlNode.HasChildNodes)
                {
                    value += xmlNode.InnerXml;
                }
                else
                {
                    value += xmlNode.InnerText;
                }
            }

            return value;
        }
    }
}
