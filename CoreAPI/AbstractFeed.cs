﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel.Syndication;
using System.Linq;

namespace Saturn.Core.API
{
    public static class SyndicationFeedExtensions
    {
        public static SyndicationLink GetSelfLink(this SyndicationFeed feed)
        {
            return feed.Links.FirstOrDefault(l => l.RelationshipType.Equals("self"));
        }

        public static SyndicationLink GetViaLink(this SyndicationFeed feed)
        {
            return feed.Links.FirstOrDefault(l => l.RelationshipType.Equals("via"));
        }

        public static SyndicationLink GetPrevArchiveLink(this SyndicationFeed feed)
        {
            return feed.Links.FirstOrDefault(l => l.RelationshipType.Equals("prev-archive"));
        }

        public static SyndicationLink GetNextArchiveLink(this SyndicationFeed feed)
        {
            return feed.Links.FirstOrDefault(l => l.RelationshipType.Equals("next-archive"));
        }
    }

    public abstract class AbstractFeed<T> : IFeed<T>
    {
        protected readonly Uri baseAddress;

        protected readonly UriTemplate feedTemplate;

        protected readonly UriTemplate entryTemplate;

        private readonly UriTemplate schemeTemplate;

        public AbstractFeed(Uri baseAddress, UriTemplate feedTemplate, UriTemplate entryTemplate)
        {
            if (baseAddress == null)
            {
                throw new ArgumentNullException("baseAddress");
            }
            this.baseAddress = baseAddress;

            if (feedTemplate == null)
            {
                throw new ArgumentNullException("feedTemplate");
            }
            this.feedTemplate = feedTemplate;

            if (entryTemplate == null)
            {
                throw new ArgumentNullException("entryTemplate");
            }
            this.entryTemplate = entryTemplate;

            // scheme template
            this.schemeTemplate = new UriTemplate("/scheme/{name}");
        }

        protected string GetId(T obj)
        {
            return string.Format("{0}_{1}", typeof(T).Name, obj.GetHashCode());
        }

        protected SyndicationLink CreateEntrySelfLink(IDictionary<string, string> parameters)
        {
            Uri uri = entryTemplate.BindByName(baseAddress, parameters);
            return SyndicationLink.CreateSelfLink(uri);
        }

        protected SyndicationCategory CreateCategory(string categoryScheme, string categoryStr)
        {
            Uri schemeUri = schemeTemplate.BindByPosition(baseAddress, categoryScheme);
            return new SyndicationCategory(categoryStr, schemeUri.ToString(), categoryStr);
        }

        protected virtual SyndicationContent CreateContent(T obj)
        {
            return new XmlSyndicationContent(GetContentType(), obj, new DataContractSerializer(typeof(T)));
        }

        protected abstract SyndicationFeed GetFeed();

        protected abstract string GetContentType();

        public abstract string AddEntry(T obj);

        public abstract void Store();
    }
}
