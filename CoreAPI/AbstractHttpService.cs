﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Saturn.Core.API
{
    public abstract class AbstractHttpService : IHttpService
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AbstractHttpService()
        {
            log.Debug("Creating instance...");
        }

        public abstract Dictionary<string, string> GetHeaders();

        public HttpResult Post(Uri uri, Stream stream, string data)
        {
            HttpResult result = new HttpResult();
            result.StartTime = TimeProvider.Current.UtcNow;
            
            bool beforeEmptyLine = true;
            string responseString = string.Empty;
            
            StringBuilder httpProtocol = new StringBuilder();
            httpProtocol.Append("POST " + uri.AbsolutePath + " HTTP/1.1" + Environment.NewLine);
            httpProtocol.Append("Host: " + uri.Host + Environment.NewLine);
            httpProtocol.Append("User-Agent: Saturn HttpService " + Environment.NewLine);
            httpProtocol.Append("Accept: */*" + Environment.NewLine);
            httpProtocol.Append("Content-Type: application/x-www-form-urlencoded" + Environment.NewLine);
            httpProtocol.Append("Content-Length: " + data.Length + Environment.NewLine);

            Dictionary<string, string> headers = GetHeaders();
            foreach (string key in headers.Keys)
            {
                httpProtocol.Append(key + ": " + headers[key] + Environment.NewLine);
            }

            string requestString = httpProtocol.ToString() + Environment.NewLine + data;
            result.Request = MaskRequestData(requestString);

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] bytes = encoding.GetBytes(requestString);
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();

            StreamReader rdr = new StreamReader(stream);
            while (rdr.Peek() != -1)
            {
                string line = rdr.ReadLine();
                if (String.IsNullOrEmpty(line))
                {
                    beforeEmptyLine = false;
                }
                else
                {
                    if (!beforeEmptyLine)
                    {
                        responseString += line + Environment.NewLine;
                    }
                }
            }

            result.Response = responseString;
            result.EndTime = TimeProvider.Current.UtcNow;

            return result;
        }

        protected abstract string MaskRequestData(string rawRequest);

        public TcpClient GetTcpClient(Uri uri)
        {
            TcpClient client;

            if (uri.Scheme.Equals("https"))
            {
                client = new TcpClient(uri.Host, 443);
            }
            else
            {
                client = new TcpClient(uri.Host, 80);
            }

            return client;
        }

        public Stream OpenStream(Uri uri, System.Net.Sockets.TcpClient tcpClient)
        {
            Stream stream;

            if (uri.Scheme.Equals("https"))
            {
                stream = new SslStream(tcpClient.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);
                ((SslStream)stream).AuthenticateAsClient(uri.Host);
            }
            else
            {
                stream = tcpClient.GetStream();
            }

            return stream;
        }

        private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                return true;
            }

            // Do not allow this client to communicate with unauthenticated servers. 
            return false;
        }
    }
}
