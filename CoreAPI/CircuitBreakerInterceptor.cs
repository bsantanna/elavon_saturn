﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;

namespace Saturn.Core.API
{
    public class CircuitBreakerInterceptor : IInterceptor
    {
        private readonly ICircuitBreaker breaker;

        private ISet<string> safeMethods = new HashSet<string>();

        public CircuitBreakerInterceptor(ICircuitBreaker breaker)
        {
            if (breaker == null)
            {
                throw new ArgumentNullException("breaker");
            }
            this.breaker = breaker;

            // set of safe methods
            safeMethods.Add("ToString");
        }

        public void Intercept(IInvocation invocation)
        {
            if (!safeMethods.Contains(invocation.Method.Name))
            {
                this.breaker.Guard();
            }
            
            try
            {
                invocation.Proceed();
                this.breaker.Succeed();
            }
            catch (Exception e)
            {
                this.breaker.Trip(e);
                throw;
            }
        }
    }
}
