﻿
namespace Saturn.Core.API
{
    public interface IMailService
    {
        void SendMail(string to, string subject, string body);
    }
}
