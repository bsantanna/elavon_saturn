﻿using System.Collections.Generic;
using System.ServiceModel.Syndication;

namespace Saturn.Core.API
{
    public interface IFeed<T>
    {
        string AddEntry(T entry);
        void Store();
    }
}
