﻿using System;
using System.IO;
using System.Net.Sockets;

namespace Saturn.Core.API
{
    public interface IHttpService
    {
        HttpResult Post(Uri uri, Stream stream, string data);

        TcpClient GetTcpClient(Uri uri);

        Stream OpenStream(Uri uri, TcpClient tcpClient);
    }
}
