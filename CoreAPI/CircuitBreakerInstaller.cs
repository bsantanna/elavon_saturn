﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;

namespace Saturn.Core.API
{
    public class CircuitBreakerInstaller : IWindsorInstaller
    {

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<ICircuitBreaker>()
                    .ImplementedBy<CircuitBreaker>()
                    .DependsOn(new { timeout = TimeSpan.FromSeconds(30) })
            );
            container.Register(Component.For<CircuitBreakerInterceptor>());
        }
    }
}
