﻿using log4net;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;

namespace Saturn.Core.API
{
    public abstract class AbstractMailService : IMailService
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SendMail(string to, string subject, string body)
        {
            var toAddress = new MailAddress(to);
            var fromAddress = GetFrom();

            var smtpClient = new SmtpClient
            {
                Host = GetHost(),
                Port = GetPort(),
                EnableSsl = IsSSLEnabled(),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, GetPassword())
            };

            var message = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body };
            log.Debug("Sending e-mail message with hashCode [" + message.GetHashCode() + "]");

            smtpClient.SendCompleted += (s, e) =>
            {
                // Get the unique identifier for this asynchronous operation.
                int hashCode = (int)e.UserState;

                if (e.Cancelled)
                {
                    log.Debug("[" + hashCode + "] Send canceled.");
                }
                if (e.Error != null)
                {
                    log.Error("[" + hashCode + "] Error: " + e.Error.ToString());
                }
                else
                {
                    log.Debug("[" + hashCode + "] Message sent.");
                }
                smtpClient.Dispose();
                message.Dispose();
            };

            smtpClient.SendAsync(message, message.GetHashCode());
        }

        protected abstract MailAddress GetFrom();

        protected abstract string GetPassword();

        protected abstract bool IsSSLEnabled();

        protected abstract int GetPort();

        protected abstract string GetHost();
    }
}
