﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net.Config;
using System;
using System.IO;
using System.Linq;

namespace Saturn.Core.API.Test
{
    public abstract class AbstractDITest
    {

        private readonly IWindsorContainer container;

        public AbstractDITest(IWindsorInstaller[] installers)
        {
            // install DI Container configuration
            this.container = new WindsorContainer();
            this.container.Install(installers);

            // Configure log4net using App.Config
            XmlConfigurator.Configure();
        }

        protected void RegisterHttpService(Type httpService)
        {
            if (httpService.GetInterfaces().Contains(typeof(IHttpService)))
            {
                var container = GetContainer();
                container.Register(
                    Component
                        .For<IHttpService>()
                        .ImplementedBy(httpService)
                );
            }
            else
            {
                throw new ArgumentException("Argument does not implement IHttpService");
            }
        }

        protected IWindsorContainer GetContainer()
        {
            return this.container;
        }

        protected DirectoryInfo CleanDirectory(string workingDir)
        {
            if (!Directory.Exists(workingDir))
            {
                Directory.CreateDirectory(workingDir);
            }

            DirectoryInfo dirInfo = new DirectoryInfo(workingDir);
            foreach (FileInfo file in dirInfo.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in dirInfo.GetDirectories())
            {
                dir.Delete(true);
            }

            return dirInfo;
        }

    }
}
