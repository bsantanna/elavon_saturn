﻿using System;
using System.Xml;
using Saturn.Core.API.Test;
using Castle.MicroKernel.Registration;
using NUnit.Framework;

namespace Saturn.Core.API
{
    [TestFixture]
    public class XmlServiceTest : AbstractDITest
    {
        private readonly string xmlString = "<test><a>X</a><b>Y</b></test>";

        public XmlServiceTest() : base(new IWindsorInstaller[] { new ServiceInstaller() }) { }

        [Test]
        public void TestGetXmlDocument()
        {
            IXmlService xmlService = this.GetContainer().Resolve<IXmlService>();
            XmlDocument xmlDocument = xmlService.GetXmlDocument(this.xmlString);
            Assert.IsNotNull(xmlDocument);
        }

        [Test]
        public void TestGetValue()
        {
            IXmlService xmlService = this.GetContainer().Resolve<IXmlService>();
            XmlDocument xmlDocument = xmlService.GetXmlDocument(this.xmlString);
            string aVal = xmlService.GetValue(xmlDocument, "/test/a");
            string bVal = xmlService.GetValue(xmlDocument, "/test/b");
            Assert.AreEqual("X", aVal);
            Assert.AreEqual("Y", bVal);
        }

    }
}
