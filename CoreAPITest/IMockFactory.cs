﻿namespace Saturn.Core.API.Test
{
    public interface IMockFactory<T>
    {
        T GetMock();

        T GetMock(object uniqueIdentifier);
    }
}
