SET "DOT_NET_HOME=C:\Windows\Microsoft.NET\Framework64\v4.0.30319\"
SET "TARGET_CONFIGURATION=Debug"

IF DEFINED WORKSPACE (
    SET "WORKING_DIRECTORY=%WORKSPACE%\%JOB_NAME%"
) ELSE (
    SET "WORKING_DIRECTORY=C:\Projetos\Solar\Saturn"
)

cmd /C "del /s /q C:\WSGateServices\Saturn"
cmd /C "rmdir /S /Q C:\WSGateServices\Saturn"
cmd /C "mkdir C:\WSGateServices\Saturn\CaptureWorker"
cmd /C "mkdir C:\WSGateServices\Saturn\CaptureSyncWorker"

cd C:\WSGateServices\Saturn\CaptureWorker
xcopy "%WORKING_DIRECTORY%\Worker\CaptureWorker\bin\%TARGET_CONFIGURATION%" /e
cmd /C "%DOT_NET_HOME%InstallUtil.exe -u C:\WSGateServices\Saturn\CaptureWorker\CaptureWorker.exe"
cmd /C "%DOT_NET_HOME%InstallUtil.exe -i C:\WSGateServices\Saturn\CaptureWorker\CaptureWorker.exe"

cd C:\WSGateServices\Saturn\CaptureSyncWorker
xcopy "%WORKING_DIRECTORY%\Worker\CaptureSyncWorker\bin\%TARGET_CONFIGURATION%" /e
cmd /C "%DOT_NET_HOME%InstallUtil.exe -u C:\WSGateServices\Saturn\CaptureSyncWorker\CaptureSyncWorker.exe"
cmd /C "%DOT_NET_HOME%InstallUtil.exe -i C:\WSGateServices\Saturn\CaptureSyncWorker\CaptureSyncWorker.exe"

cmd /C "icacls C:\WSGateServices /grant *S-1-1-0:(OI)(CI)F /C /T"

cd %WORKING_DIRECTORY%
