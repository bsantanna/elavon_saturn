# Saturn

Saturn é um projeto [.NET Framework](https://en.wikipedia.org/wiki/.NET_Framework) 4.5, sua motivação inicial foi a reimplementação do serviço de captura.
Este projeto faz forte utilização de design patterns com ênfase em [desenvolvimento orientado a testes](https://en.wikipedia.org/wiki/Test-driven_development) e reutilização de código por meio de [injeção de dependências](https://en.wikipedia.org/wiki/Dependency_injection).
Sua implementação foi baseada no excelente livro [Dependency Injection in .NET de Mark Seemann](http://www.manning.com/seemann/), leitura recomendada.


## Bibliotecas externas
Para o projeto Saturn as seguintes bibliotecas são utilizadas:

- [Apache log4net](https://logging.apache.org/log4net/): Framework de log da aplicação
- [Castle Windsor](http://www.castleproject.org/projects/windsor/): Framework de injeção de dependências e criação de proxyes dinâmicos (magia negra)
- [NUnit](http://www.nunit.org/): Framework de testes unitários

Logo após clonar o projeto o build não vai funcionar, as referências estarão corretas porém os arquivos dll dessas bibliotecas não estarão presentes no projeto pois é uma prática ruim fazer commit de arquivos binários, sistemas de gerenciamento de código não devem ser utilizados para essa finalidade, para corrigir esse problema no Visual Studio abra o menu:
```
Tools > NuGet Package Manager > Package Manager Console
```

Neste console execute os seguintes comandos (um de cada vez):
```
Update-Package –reinstall Castle.Windsor
```

```
Update-Package –reinstall log4net
```

```
Update-Package -reinstall NUnit
```

O build deverá funcionar corretamente agora...

## Scripts de automatização de tarefas
Com a finalidade de automatizar tarefas repetitivas, existem dois scripts no formato .bat dentro do diretório raiz do projeto:

- **build.bat** Cria os arquivos binários (.dll, .exe, etc...)
- **deploy.bat** Copia os arquivos binários gerados pelo script *build.bat* para as máquinas virtuais do projeto [Jupiter](https://bitbucket.org/bsantanna/elavon_jupiter)

Utilize estes scripts para ajudar no ciclo de desenvolvimento da aplicação.

## Máquina virtual Titan
A máquina virtual [Titan](https://en.wikipedia.org/wiki/Titan_(moon)) é utilizada para desenvolvimento da camada web do projeto Saturn.
Para instalar esta máquina virtual utilize o seguinte comando:
```
vagrant box add --name elavon/titan --provider virtualbox <Caminho>\package.box
```
