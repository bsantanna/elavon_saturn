SET "SSH_PORT=22"
SET "SSH_SERVICES_HOST=vagrant@172.16.0.12"
SET "SSH_SERVICES_DESTINATION=%SSH_SERVICES_HOST%:C:\WSGateServices\Saturn\"
SET "DOT_NET_HOME=C:\Windows\Microsoft.NET\Framework64\v4.0.30319\"
SET "TARGET_CONFIGURATION=Debug"

IF DEFINED WORKSPACE (
    SET "WORKING_DIRECTORY=%WORKSPACE%\%JOB_NAME%"
) ELSE (
    SET "WORKING_DIRECTORY=C:\Projetos\Solar\Saturn"
)

ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "del /s /q C:\WSGateServices\Saturn"
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "rmdir /S /Q C:\WSGateServices\Saturn"
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "mkdir C:\WSGateServices\Saturn"

cd "%WORKING_DIRECTORY%\Worker\CaptureWorker\bin"
scp -r -P %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key "%TARGET_CONFIGURATION%" %SSH_SERVICES_DESTINATION%
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "move C:\WSGateServices\Saturn\%TARGET_CONFIGURATION% C:\WSGateServices\Saturn\CaptureWorker"
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "%DOT_NET_HOME%InstallUtil.exe -u C:\WSGateServices\Saturn\CaptureWorker\CaptureWorker.exe"
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "%DOT_NET_HOME%InstallUtil.exe -i C:\WSGateServices\Saturn\CaptureWorker\CaptureWorker.exe"

cd "%WORKING_DIRECTORY%\Worker\CaptureSyncWorker\bin"
scp -r -P %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key "%TARGET_CONFIGURATION%" %SSH_SERVICES_DESTINATION%
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "move C:\WSGateServices\Saturn\%TARGET_CONFIGURATION% C:\WSGateServices\Saturn\CaptureSyncWorker"
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "%DOT_NET_HOME%InstallUtil.exe -u C:\WSGateServices\Saturn\CaptureSyncWorker\CaptureSyncWorker.exe"
ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "%DOT_NET_HOME%InstallUtil.exe -i C:\WSGateServices\Saturn\CaptureSyncWorker\CaptureSyncWorker.exe"


ssh -p %SSH_PORT% -i %WORKING_DIRECTORY%/insecure_private_key %SSH_SERVICES_HOST% cmd /C "icacls C:\WSGateServices /grant *S-1-1-0:(OI)(CI)F /C /T"

cd %WORKING_DIRECTORY%
