﻿using System;
using Saturn.Worker.Common;
using NUnit.Framework;

namespace Saturn.Worker.Common
{
    [TestFixture]
    public class BusinessConstantsTest
    {
        [Test]
        public void TestGetDescriptionValue()
        {
            Assert.AreEqual("AA", BusinessConstants.GetDescriptionValue(BusinessConstants.ResponseCode.Approved));
        }
    }
}
