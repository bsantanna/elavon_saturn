﻿using Saturn.Worker.Domain;
using System;
using System.Linq;
using System.ServiceModel.Syndication;

namespace Saturn.Worker.Feed.Test
{
    public class ConsoleWorkSummaryFeedParser:AbstractWorkSummaryFeedParser
    {

        public ConsoleWorkSummaryFeedParser(IWorkerContext workerContext) : base(workerContext) { }

        protected override void ProcessBatchDetail(BatchDetail batchDetail, bool batchDetailIsValid)
        {
            Console.WriteLine("BatchDetail.TransactionID => " + batchDetail.TransactionID +  ", batchDetailIsValid = " + batchDetailIsValid);
        }

        protected override void ProcessBatchDetailFeed(SyndicationFeed batchDetailFeed, bool batchIsValid)
        {
            Console.WriteLine("BatchDetailFeed.Id => " + batchDetailFeed.Id);
            Batch batch = batchDetailFeed.ElementExtensions.ReadElementExtensions<Batch>("Batch", "http://schemas.elavon.com.br").Single();
            Console.WriteLine("Batch.BatchId => " + batch.BatchId + ", batchIsValid = " + batchIsValid);
        }

        protected override void ProcessBatchFeed(SyndicationFeed batchFeed)
        {
            Console.WriteLine("BatchFeed.Id => " + batchFeed.Id);
        }
    }
}
