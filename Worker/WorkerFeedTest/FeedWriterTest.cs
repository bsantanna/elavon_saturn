﻿using System;
using Saturn.Worker.Domain.Test;
using Castle.MicroKernel.Registration;
using Saturn.Core.API;
using Saturn.Worker.Common;
using Saturn.Worker.Error;
using Saturn.Worker.Logs;
using Saturn.Worker.Domain;
using System.Linq;
using Castle.DynamicProxy;
using Saturn.Core.API.Test;
using Saturn.Worker.Data.Binary;
using System.IO;
using System.ServiceModel.Syndication;
using System.Collections.Generic;
using NUnit.Framework;

namespace Saturn.Worker.Feed
{
    [TestFixture]
    public class FeedWriterTest : AbstractWorkerTest
    {
        public FeedWriterTest()
            : base(new IWindsorInstaller[] { new WorkerUnitTestInstaller(), new ServiceInstaller(), new CircuitBreakerInstaller(), new WorkerCommonInstaller(), new WorkerErrorHandlingInstaller(), new WorkerLogsInstaller(), new WorkerFeedInstaller() }) { }


        [Test]
        public void TestDepedencyResolve()
        {
            IWorker feedWriter = GetFeedWriter();
            Assert.IsNotNull(feedWriter);
        }


        [Test]
        public void TestWork()
        {
            var container = GetContainer();
            IMockFactory<WorkSummary> mockFactory = container.Resolve<IMockFactory<WorkSummary>>();

            ICollection<WorkSummary> workSummaryCollection = new List<WorkSummary>();
            for (int i = 0; i < 10; i++)
            {
                WorkSummary workSummary = mockFactory.GetMock(i);
                workSummary.EndTime = TimeProvider.Current.UtcNow;
                workSummaryCollection.Add(workSummary);
            }

            FileWorkSummaryFeedRepository workSummaryFeedRepository = container.Resolve<IFeedRepository<WorkSummary>>() as FileWorkSummaryFeedRepository;
            string directory = workSummaryFeedRepository.GetWorkingDirectory();
            CleanDirectory(directory);

            string feedPath = workSummaryFeedRepository.GetWorkingFeedPath(null);
            Assert.IsFalse(File.Exists(feedPath));

            IWorker feedWriter = GetFeedWriter();
            foreach (WorkSummary workSummary in workSummaryCollection)
            {
                feedWriter.Work(workSummary);
            }

            Assert.IsTrue(File.Exists(feedPath));
        }

        [Test]
        public void TestArchiveLink()
        {
            DateTime threeDaysAgo = TimeProvider.Current.UtcNow.AddDays(-3);

            FileWorkSummaryFeedRepository workSummaryFeedRepository = GetContainer().Resolve<IFeedRepository<WorkSummary>>() as FileWorkSummaryFeedRepository;
            string directory = workSummaryFeedRepository.GetWorkingDirectory();
            CleanDirectory(directory);

            string day = threeDaysAgo.ToString("dd");
            string month = threeDaysAgo.ToString("MM");
            string year = threeDaysAgo.ToString("yyyy");

            string archiveDirectory = Path.Combine(workSummaryFeedRepository.GetWorkingDirectory(), year, month, day);
            Directory.CreateDirectory(archiveDirectory);

            string archiveFeed = Path.Combine(archiveDirectory, "feed.xml");
            File.WriteAllText(archiveFeed, "" +
                "﻿<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<feed xmlns=\"http://www.w3.org/2005/Atom\">" +
                    "<title type=\"text\">WorkSummary</title>" +
                    "<id>urn:uuid:b122ddba-cc55-4ac3-a5cf-7f65ce6f5f61</id>" +
                    "<updated>" + year + "-" + month + "-" + day + "T15:36:32Z</updated>" +
                    "<author>" +
                       "<name>Test</name>" +
                    "</author>" +
                    "<generator>Test</generator>" +
                    "<link rel=\"self\" href=\"http://localhost/test/recent\" />" +
                    "<link rel=\"via\" href=\"http://localhost/test/" + year + "/" + month + "/" + day + "/feed.xml\" />" +
                "</feed>");

            IWorker feedWriter = GetFeedWriter();
            bool foundArchive = false;
            SyndicationFeed feed = workSummaryFeedRepository.GetWorkingFeed(new Uri("http://localhost/test"), new UriTemplate("test"), null);
            foreach (SyndicationLink link in feed.Links)
            {
                if ("prev-archive".Equals(link.RelationshipType))
                {
                    foundArchive = true;
                }
            }
            Assert.IsTrue(foundArchive);
        }


        private IWorker GetFeedWriter()
        {
            var container = GetContainer();
            IWorker[] workers = container.ResolveAll<IWorker>();
            IWorker feedWriter = null;

            foreach (IWorker worker in workers)
            {
                Type[] interfaces = worker.GetType().GetInterfaces();
                if (interfaces.Contains(typeof(IProxyTargetAccessor)))
                {
                    IProxyTargetAccessor proxy = worker as IProxyTargetAccessor;
                    object target = proxy.DynProxyGetTarget();
                    if (target is FeedWriter)
                    {
                        feedWriter = worker;
                    }
                }
                else if (worker is FeedWriter)
                {
                    feedWriter = worker;
                }
            }

            return feedWriter;
        }
    }
}
