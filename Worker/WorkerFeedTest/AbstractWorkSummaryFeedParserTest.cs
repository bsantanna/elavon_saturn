﻿using System;
using Saturn.Core.API.Test;
using Castle.MicroKernel.Registration;
using Saturn.Worker.Domain.Test;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed.Test;
using System.Text;
using Saturn.Core.API;
using System.Collections.Generic;
using NUnit.Framework;

namespace Saturn.Worker.Feed
{
    [TestFixture]
    public class AbstractWorkSummaryFeedParserTest : AbstractDITest 
    {
        public AbstractWorkSummaryFeedParserTest() : base(new IWindsorInstaller[] { new WorkerUnitTestInstaller() }) { }

        //[TestMethod] // uncoment for testing, Mimas must be online
        public void TestProcess()
        {
            var container = GetContainer();
            IFeedParser feedParser = new ConsoleWorkSummaryFeedParser(container.Resolve<IWorkerContext>());
            Uri feedUri = feedParser.GetFeedUri();
            feedParser.ProcessFeed(feedUri);
        }

        [Test]
        public void TestGetResponseDictionary()
        {
            var container = GetContainer();
            IFeedParser feedParser = new ConsoleWorkSummaryFeedParser(container.Resolve<IWorkerContext>());
            HttpResult mockHttpResult = new HttpResult();
            mockHttpResult.Response = "A=a\r\nB=b\r\n";
            IDictionary<string, string> responseDictionary = feedParser.GetResponseDictionary(mockHttpResult);
            Assert.AreEqual("a", responseDictionary["A"]);
            Assert.AreEqual("b", responseDictionary["B"]);
        }
    }
}
