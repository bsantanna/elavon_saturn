﻿using System.Collections.Generic;
using System;
using log4net;

namespace Saturn.Worker.Domain
{
    public class BatchService : IBatchService
    {
        private readonly IBatchRepository batchRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IWorkSummaryRepository workSummaryRepository;
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public BatchService(IBatchRepository batchRepository, IScheduleRepository scheduleRepository, IWorkSummaryRepository workSummaryRepository)
        {
            log.Debug("Creating instance...");

            if (batchRepository == null)
            {
                throw new ArgumentNullException("batchRepository");
            }
            this.batchRepository = batchRepository;

            if (scheduleRepository == null)
            {
                throw new ArgumentNullException("scheduleRepository");
            }
            this.scheduleRepository = scheduleRepository;

            if (workSummaryRepository == null)
            {
                throw new ArgumentNullException("workSummaryRepository");
            }
            this.workSummaryRepository = workSummaryRepository;
        }

        public ICollection<Batch> GetBatches()
        {
            ICollection<Batch> batches = null;
            try
            {
                batches = this.batchRepository.GetBatches();
            }
            catch (Exception e)
            {
                log.Error("Error while recovering batches", e);
            }
            finally
            {
                if (batches == null)
                {
                    batches = new HashSet<Batch>();
                }
            }

            return batches;
        }

        public ICollection<WorkSummary> GetPendingWork()
        {
            ICollection<WorkSummary> pendingWork = null;
            try
            {
                pendingWork = this.workSummaryRepository.GetPendingWork();
            }
            catch (Exception e)
            {
                log.Error("Error while recovering pending work.", e);
            }
            finally
            {
                if (pendingWork == null)
                {
                    pendingWork = new HashSet<WorkSummary>();
                }
            }

            return pendingWork;
        }

        public string StoreWorkSummary(WorkSummary workSummary)
        {
            string path = "";

            if (workSummary.ProcessedBatches.Count > 0 || workSummary.PendingBatches.Count > 0 || workSummary.InvalidBatches.Count > 0)
            {
                path += this.workSummaryRepository.Store(workSummary);
            }

            return path;
        }

        public bool ShouldExecute()
        {
            try
            {
                Schedule schedule = this.scheduleRepository.GetSchedule();
                if (schedule != null && !schedule.ForceStop)
                {
                    if (schedule.ForceExecute || schedule.IsOnTime())
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool NotifyCompletion(DateTime dateTime, bool isOK)
        {
            try
            {
                Schedule schedule = this.scheduleRepository.GetSchedule();
                if (schedule != null)
                {
                    schedule.LastExecution = dateTime;
                    if (isOK)
                    {
                        schedule.LastExecutionOK = dateTime;
                    }
                    this.scheduleRepository.UpdateSchedule(schedule);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool NotifyExecution(DateTime dateTime)
        {
            try
            {
                Schedule schedule = this.scheduleRepository.GetSchedule();
                if (schedule != null)
                {
                    schedule.LastExecutionTimer = dateTime;
                    this.scheduleRepository.UpdateSchedule(schedule);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
