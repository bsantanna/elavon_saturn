﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Saturn.Worker.Domain
{
    [Serializable]
    public class WorkSummary
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public ICollection<Batch> InvalidBatches { get; private set; }
        public Queue<Batch> PendingBatches { get; private set; }
        public ICollection<Batch> ProcessedBatches { get; private set; }
        public readonly string WorkerName;

        public WorkSummary(IWorkerContext workerContext)
        {
            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.WorkerName = workerContext.WorkerName();
            this.InvalidBatches = new HashSet<Batch>();
            this.PendingBatches = new Queue<Batch>();
            this.ProcessedBatches = new HashSet<Batch>();
        }
    }
}
