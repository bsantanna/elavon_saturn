﻿using log4net;
using Saturn.Core.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Timers;

namespace Saturn.Worker.Domain
{
    public abstract class AbstractWorker : ServiceBase, IWorker
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly System.Timers.Timer timer;
        private System.ComponentModel.Container components = null;
        private bool notificationOnline;
        protected bool workInProgress;
        protected readonly IMailService mailService;
        protected readonly IBatchService batchService;
        protected readonly IWorkerContext workerContext;
        protected readonly IWorker nextWorker;
        protected EventLog eventLog;

        public AbstractWorker(IWorkerContext workerContext, IMailService mailService, IBatchService batchService, IWorker nextWorker)
        {
            log.Debug("Creating instance...");

            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }
            this.mailService = mailService;

            if (batchService == null)
            {
                throw new ArgumentNullException("batchService");
            }
            this.batchService = batchService;

            if (nextWorker == null)
            {
                throw new ArgumentNullException("nextWorker");
            }
            this.nextWorker = nextWorker;

            // setup timer
            int interval = 30;
            this.timer = new System.Timers.Timer();
            this.timer.Elapsed += new ElapsedEventHandler(TimerCallback);
            this.timer.Interval = interval * 1000;

            // class scoped vars
            this.notificationOnline = true;
            this.workInProgress = false;

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.eventLog = new EventLog();
            ((ISupportInitialize)(this.eventLog)).BeginInit();

            string eventLogSource = "Worker_" + workerContext.WorkerName() + "_Source";

            if (!EventLog.SourceExists(eventLogSource))
            {
                EventLog.CreateEventSource(eventLogSource, workerContext.WorkerName());
            }

            this.eventLog.Source = eventLogSource;
            this.eventLog.Log = workerContext.WorkerName();
            this.ServiceName = "WSGate.Services." + workerContext.WorkerName();

            ((ISupportInitialize)(this.eventLog)).EndInit();
        }

        public IWorkerContext GetWorkerContext()
        {
            return this.workerContext;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        protected override void OnContinue()
        {
            this.log.Info("Continue called...");
            base.OnContinue();
            this.timer.Enabled = true;
        }

        protected override void OnPause()
        {
            this.log.Info("Pause called...");
            this.timer.Enabled = false;
            while(workInProgress)
            {
                this.log.Warn("Work in progress, will try wait for a while...");
                Thread.Sleep(5000);
            }
            base.OnPause();
        }

        protected override void OnStart(string[] args)
        {
            string logMessage = "Starting execution in " + (this.timer.Interval / 1000) + " seconds...";
            this.eventLog.WriteEntry(logMessage);
            this.log.Info(logMessage);

            this.timer.Enabled = true;
            this.timer.Start();
        }

        protected override void OnStop()
        {
            this.log.Info("Stop called...");
            this.timer.Enabled = false;
            while (workInProgress)
            {
                this.log.Warn("Work in progress, will try wait for a while...");
                Thread.Sleep(5000);
            }
            base.OnStop();
        }

        private void SendOfflineEmail()
        {
            string subject = "[ERROR] Database is offline for Worker " + this.workerContext.WorkerName();
            string body = "Could not connect to database for Worker " + this.workerContext.WorkerName()
                + ". Application will keep trying to execute, as soon as it recover connection another e-mail will be sent.";
            this.mailService.SendMail(workerContext.NotificationEmailAddress(), subject, body);
        }

        private void SendOnlineEmail()
        {
            string subject = "[OK] Database is online for Worker " + this.workerContext.WorkerName();
            string body = "Communication with database is online for Worker " + this.workerContext.WorkerName()
                + ". Application will keep execution as planned.";
            this.mailService.SendMail(workerContext.NotificationEmailAddress(), subject, body);
        }

        private void TimerCallback(object source, ElapsedEventArgs e)
        {
            // disable timer execution (only one execution per instance)
            this.timer.Enabled = false;

            // notify execution time
            bool notified = this.batchService.NotifyExecution(TimeProvider.Current.UtcNow);
            if (!notified)
            {
                log.Warn("Could not update Tasker at this time for Worker[" + this.workerContext.WorkerName() + "], application is running as planned...");

                if (notificationOnline)
                {
                    // could not notify execution, probably database is offline notify team by e-mail
                    this.SendOfflineEmail();
                    notificationOnline = false;
                }
            }
            else
            {
                if (!notificationOnline)
                {
                    // connection restabilished
                    log.Info("Tasker updated for Worker[" + this.workerContext.WorkerName() + "], which means application recovered connection with database (some previous execution failed).");
                    this.SendOnlineEmail();
                    notificationOnline = true;
                }

                // verify execution
                if (ShouldExecute())
                {
                    this.workInProgress = true;
                    this.Start();
                    this.workInProgress = false;
                }
            }

            // enable timer execution
            this.timer.Enabled = true;
        }

        public void Start()
        {
            // recover pending work from batchService
            Queue<WorkSummary> pendingWork = new Queue<WorkSummary>();
            foreach (WorkSummary workSummary in this.batchService.GetPendingWork())
            {
                pendingWork.Enqueue(workSummary);
            }

            // enqueue next empty work
            WorkSummary nextWork = new WorkSummary(workerContext);
            nextWork.StartTime = TimeProvider.Current.UtcNow;
            pendingWork.Enqueue(nextWork);

            // starts queue iteration
            while (pendingWork.Count > 0)
            {
                // perform work
                var currentWork = pendingWork.Peek();
                this.Work(currentWork);

                if (currentWork.PendingBatches.Count > 0)
                {
                    // log status message
                    int timeout = this.workerContext.ViaConnexRetryTimeout();
                    log.Warn("Last execution of Worker[" + this.workerContext.WorkerName() + "] did not complete processing batches.");
                    log.Warn("There are " + currentWork.PendingBatches.Count + " pending batches to be processed.");
                    log.Warn("Will retry to process pending work in " + timeout + " seconds, currently there are " + pendingWork.Count + " jobs to be processed.");

                    // sleep for a while before trying again (will not dequeue items)
                    Thread.Sleep(timeout * 1000);
                    log.Warn("Proceeding execution...");

                    // update last execution with not ok flag
                    log.Warn("Notifying Tasker about non successfull execution...");
                    this.batchService.NotifyCompletion(TimeProvider.Current.UtcNow, false);
                }
                else
                {
                    if (currentWork.PendingBatches.Count <= 0 && currentWork.ProcessedBatches.Count <= 0 && currentWork.InvalidBatches.Count <= 0)
                    {
                        log.Info("Worker[" + this.workerContext.WorkerName() + "] execution result is an empty WorkSummary (without batches), will not propagate WorkSummary.");
                    }
                    else
                    {
                        // there are some processed batches in this worksummary (either valid or invalid), propagating work...
                        this.End(currentWork);
                    }

                    // dequeue pending work from queue
                    pendingWork.Dequeue();
                }
            }

            // notify completion
            bool notified = this.batchService.NotifyCompletion(TimeProvider.Current.UtcNow, true);
            while (!notified)
            {
                if (notificationOnline)
                {
                    // could not notify execution, probably database is offline notify team by e-mail
                    this.SendOfflineEmail();
                    notificationOnline = false;
                }

                // couldnt notify completion, try again since next execution will be affected by this update
                log.Warn("Could not update Tasker at this time for Worker[" + this.workerContext.WorkerName() + "], application is running as planned...");
                Thread.Sleep(this.workerContext.ViaConnexRetryTimeout() * 1000);
                notified = this.batchService.NotifyCompletion(TimeProvider.Current.UtcNow, true);
            }

            if (notified && !notificationOnline)
            {
                log.Info("Tasker updated for Worker[" + this.workerContext.WorkerName() + "], which means application recovered connection with database (some previous execution failed).");
                this.SendOnlineEmail();
                notificationOnline = true;
            }
        }

        public void End(WorkSummary workSummary)
        {
            // store complete work
            workSummary.EndTime = TimeProvider.Current.UtcNow;
            this.batchService.StoreWorkSummary(workSummary);

            // perform next work 
            this.nextWorker.Work(workSummary);
        }

        public abstract void Work(WorkSummary workSummary);

        protected virtual bool ShouldExecute()
        {
            bool value;

            try
            {
                value = this.batchService.ShouldExecute();
            }
            catch (NullReferenceException e)
            {
                value = false;
            }

            return value;
        }
    }
}
