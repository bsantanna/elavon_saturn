﻿using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain
{
    public interface IBatchRepository
    {
        ICollection<Batch> GetBatches();
    }
}
