﻿using Saturn.Core.API;
using System;
using System.Runtime.Serialization;

namespace Saturn.Worker.Domain
{
    [Serializable]
    [DataContract(Name = "BatchDetail", Namespace = "http://schemas.elavon.com.br")]
    public class BatchDetail
    {
        [DataMember]
        public string Amount { get; set; }
        [DataMember]
        public string Request { get; set; }
        [DataMember]
        public string NSUHost { get; set; }
        [DataMember]
        public string CardProduct { get; set; }
        [DataMember]
        public string ECI { get; set; }
        [DataMember]
        public string ThreeDSAuthenticationValue { get; set; }
        [DataMember]
        public string Recurring { get; set; }
        [DataMember]
        public string InstallmentsRecurring { get; set; }
        [DataMember]
        public string Installments { get; set; }
        [DataMember]
        public string CaptureDetailsInstallments { get; set; }
        [DataMember]
        public string CaptureDetailsAmount { get; set; }
        [DataMember]
        public string PurchaseDepartureDate { get; set; }
        [DataMember]
        public string CardData { get; set; }
        [DataMember]
        public string AuthAmount { get; set; }
        [DataMember]
        public string AuthAmountCurrencyCode { get; set; }
        [DataMember]
        public string ApprovalCode { get; set; }
        [DataMember]
        public string AuthorizationDateTime { get; set; }
        [DataMember]
        public string POSEntryCapability { get; set; }
        [DataMember]
        public string CardEntryMode { get; set; }
        [DataMember]
        public string PINBlockDetails { get; set; }
        [DataMember]
        public string AuthorizationSource { get; set; }
        [DataMember]
        public string CVV2Response { get; set; }
        [DataMember]
        public string PS2000Data { get; set; }
        [DataMember]
        public string MSDI { get; set; }
        [DataMember]
        public string ICCDetails { get; set; }
        [DataMember]
        public string MerchantDynamicDBA { get; set; }
        [DataMember]
        public string PurchaseAdditionalID { get; set; }
        [DataMember]
        public string DebitEBTNetworkID { get; set; }
        [DataMember]
        public string DebitEBTSettlementDate { get; set; }
        [DataMember]
        public string DebitInterchangeIndicator { get; set; }
        [DataMember]
        public string TransactionID { get; set; }
        [DataMember]
        public HttpResult DetailHttpResult { get; set; }
    }
}
