﻿using System;

namespace Saturn.Worker.Domain
{
    public interface IWorkerContext
    {
        string WorkerName();

        string AppDataDirectory();

        string SqlConnectionUri();

        int HttpKeepAlive();

        string RegKey();

        string ViaConnexUri();

        int ViaConnexRetryTimeout();

        string NotificationEmailAddress();

        string FromEmailAddress();

        string EmailPassword();

        bool EmailSSLEnabled();

        int EmailSMTPPort();

        string EmailHost();

        string FeedBaseUri();
    }
}
