﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Domain
{
    public interface IWorker
    {
        void Start();

        void End(WorkSummary workSummary);

        void Work(WorkSummary workSummary);

        IWorkerContext GetWorkerContext();
    }
}
