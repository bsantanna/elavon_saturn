﻿namespace Saturn.Worker.Domain
{
    public interface IBatchTransformer<T>
    {
        T Transform(Batch batch);
    }
}
