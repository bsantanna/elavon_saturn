﻿using Saturn.Core.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Domain
{
    public class Schedule
    {
        public string ServiceID { get; set; }
        public string MinuteToRun { get; set; }
        public string HourToRun { get; set; }
        public bool ForceExecute { get; set; }
        public bool ForceStop { get; set; }
        public DateTime LastExecution { get; set; }
        public DateTime LastExecutionOK { get; set; }
        public DateTime LastExecutionTimer { get; set; }

        public bool IsOnTime()
        {
            bool validMinute = false, validHour = false;
            if (!String.IsNullOrEmpty(MinuteToRun))
            {
                validMinute = MinuteToRun.Split(';').Select(str => Int32.Parse(str.Trim())).Contains(TimeProvider.Current.UtcNow.Minute);
            }

            if (!String.IsNullOrEmpty(HourToRun))
            {
                validHour = HourToRun.Split(';').Select(str => Int32.Parse(str.Trim())).Contains(TimeProvider.Current.UtcNow.Hour);
            }

            return validMinute && validHour;
        }
    }
}
