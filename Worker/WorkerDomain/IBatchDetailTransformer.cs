﻿namespace Saturn.Worker.Domain
{
    public interface IBatchDetailTransformer<T>
    {
        T Transform(Batch batch, BatchDetail batchDetail);
    }
}
