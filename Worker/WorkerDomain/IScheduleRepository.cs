﻿using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain
{
    public interface IScheduleRepository
    {
        Schedule GetSchedule();
        void UpdateSchedule(Schedule schedule);
    }
}
