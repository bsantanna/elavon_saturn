﻿using System;

namespace Saturn.Worker.Domain
{
    public interface IBackOfficeRepository
    {
        void LogTrans(
            string terminalId,
            string transactionId,
            string paymentTransaction,
            string source,
            string responseCode,
            string responseMessage,
            string dumpRequest,
            string dumpResponse,
            string sourceIpAddress,
            DateTime dateTimeRequest,
            DateTime dateTimeResponse);

        void LogTasker(
            string terminalId,
            string status,
            string paymentTransaction,
            string responseDetails,
            string recordCount,
            DateTime taskerDateTime);

        void LogTasker(
            string terminalId,
            string transactionId,
            string status,
            string paymentTransaction,
            string responseDetails,
            string recordCount,
            DateTime taskerDateTime);

        void UpdateHeader(
            string batchId,
            string terminalId,
            string dumpHeaderRequest,
            string dumpHeaderResponse);

        void UpdateDetail(
            string batchId,
            string terminalId,
            string transactionId,
            string dumpDetailRequest,
            string dumpDetailResponse);

        void UpdateTrailer(
            string batchId,
            string terminalId,
            string dumpTrailerRequest,
            string dumpTrailerResponse);
    }
}
