﻿using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain
{
    public interface IWorkSummaryRepository
    {
        ICollection<WorkSummary> GetPendingWork();

        ICollection<WorkSummary> GetAllFromDate(DateTime date);

        string Store(WorkSummary workSummary);
    }
}
