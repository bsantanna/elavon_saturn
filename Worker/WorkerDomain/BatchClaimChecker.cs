﻿using Saturn.Core.API;
using System;
using System.Linq;
using System.Collections.Generic;
using log4net;

namespace Saturn.Worker.Domain
{
    public class BatchClaimChecker : IClaimChecker<Batch>
    {
        private readonly IWorkSummaryRepository workSummaryRepository;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public BatchClaimChecker(IWorkSummaryRepository workSummaryRepository)
        {
            log.Debug("Creating instance...");

            if (workSummaryRepository == null)
            {
                throw new ArgumentNullException("workSummaryRepository");
            }
            this.workSummaryRepository = workSummaryRepository;
        }

        public bool IsValid(Batch batch)
        {
            DateTime today = TimeProvider.Current.UtcNow;
            DateTime yesterday = today.AddDays(-1);

            IEnumerable<WorkSummary> workSummaryList = this.workSummaryRepository.GetAllFromDate(today).Union(this.workSummaryRepository.GetAllFromDate(yesterday));

            foreach (WorkSummary workSummary in workSummaryList)
            {
                foreach (Batch currentBatch in workSummary.ProcessedBatches)
                {
                    if (currentBatch.BatchId != null && currentBatch.BatchId.Equals(batch.BatchId))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
