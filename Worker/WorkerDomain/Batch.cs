﻿using Saturn.Core.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Saturn.Worker.Domain
{
    [Serializable]
    [DataContract(Name = "Batch", Namespace = "http://schemas.elavon.com.br")]
    public class Batch
    {
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string ApplicationId { get; set; }
        [DataMember]
        public string TerminalId { get; set; }
        [DataMember]
        public string BatchNumber { get; set; }
        [DataMember]
        public string BatchId { get; set; }
        public IList<BatchDetail> Details { get; set; }
        public ICollection<BatchDetail> InvalidDetails { get; set; }
        [DataMember]
        public HttpResult HeaderHttpResult { get; set; }
        [DataMember]
        public HttpResult TrailerHttpResult { get; set; }
        [DataMember]
        public readonly string ProcessorHostName;

        public Batch()
        {
            Details = new List<BatchDetail>();
            InvalidDetails = new HashSet<BatchDetail>();
            ProcessorHostName = Environment.MachineName;
        }

        public int GetTotalAmount()
        {
            int sum = 0;
            if (this.Details != null)
            {
                sum += this.Details.Sum(detail => { return Int32.Parse(detail.Amount); });
            }
            return sum;
        }
    }
}
