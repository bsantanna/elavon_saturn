﻿using log4net;
using Saturn.Core.API;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain
{
    public class BatchSegmentHttpResultValidator : IHttpResultValidator
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public BatchSegmentHttpResultValidator()
        {
            log.Debug("Creating instance...");
        }

        public bool IsValid(HttpResult result)
        {
            if (result != null && !String.IsNullOrEmpty(result.Response))
            {
                return result.Response.Contains("9F.Status=A");
            }
            return false;
        }
    }
}
