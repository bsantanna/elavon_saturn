﻿using Saturn.Core.API;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain
{
    public interface IBatchService
    {
        ICollection<Batch> GetBatches();
        ICollection<WorkSummary> GetPendingWork();
        string StoreWorkSummary(WorkSummary workSummary);
        bool ShouldExecute();
        bool NotifyCompletion(DateTime dateTime, bool isOK);
        bool NotifyExecution(DateTime dateTime);
    }
}
