﻿using Castle.DynamicProxy;
using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.IO;

namespace Saturn.Worker.Error
{
    public class StorePendingWorkInterceptor : IInterceptor
    {
        private IWorkSummaryRepository workSummaryRepository;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public StorePendingWorkInterceptor(IWorkSummaryRepository workSummaryRepository)
        {
            if (workSummaryRepository == null)
            {
                throw new ArgumentNullException("workSummaryRepository");
            }
            this.workSummaryRepository = workSummaryRepository;
        }

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                Type argumentType = null;

                if (invocation.Arguments != null && invocation.Arguments.Length > 0)
                {
                    argumentType = invocation.Arguments[0].GetType();
                }

                if (argumentType != null && argumentType.Equals(typeof(WorkSummary)))
                {
                    WorkSummary workSummary = invocation.Arguments[0] as WorkSummary;
                    string path = this.workSummaryRepository.Store(workSummary);
                    log.Warn("Stored pending work in file " + path + " for future retry attempt.");
                }

                throw;
            }
        }
    }
}
