﻿using Castle.DynamicProxy;
using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.IO;
using System.Linq;

namespace Saturn.Worker.Error
{
    public class MailNotificationInterceptor : IInterceptor
    {
        private readonly IMailService mailService;

        private readonly IWorkerContext workerContext;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MailNotificationInterceptor(IWorkerContext workerContext, IMailService mailService)
        {
            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }
            this.mailService = mailService;
        }

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                if (!(e is InvalidOperationException) && !(e is IOException))
                {
                    string subject = "[ERROR] Error with Worker => " + workerContext.WorkerName();
                    string body = "Exception Message: " + e.Message + Environment.NewLine + "Stack Trace: " + e.StackTrace;

                    log.Warn("Caught an error, notifying address " + workerContext.NotificationEmailAddress());
                    mailService.SendMail(workerContext.NotificationEmailAddress(), subject, body);
                }

                throw;
            }
        }
    }
}
