﻿using Castle.DynamicProxy;
using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Linq;

namespace Saturn.Worker.Error
{
    public class WorkerErrorHandler : IInterceptor
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                Type[] interfaces = invocation.InvocationTarget.GetType().GetInterfaces();

                // handle errors in IWorker instance
                if (interfaces.Contains(typeof(IWorker)))
                {
                    IWorker worker = invocation.InvocationTarget as IWorker;

                    log.Error("Caught an error while performing Work[" +
                            worker.GetWorkerContext().WorkerName() + "] => " +
                            invocation.Method.Name + "()", e);
                }

                // handle errors in IBatchService instance
                if (interfaces.Contains(typeof(IBatchService)))
                {
                    log.Error("Caught an error while invoking IBatchService => " + invocation.Method.Name + "()", e);
                }
            }
        }
    }
}
