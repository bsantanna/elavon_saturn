﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Error
{
    public class WorkerErrorHandlingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<MailNotificationInterceptor>());
            container.Register(Component.For<StorePendingWorkInterceptor>());
            container.Register(Component.For<WorkerErrorHandler>());
        }
    }
}
