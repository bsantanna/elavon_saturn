﻿using System;
using Saturn.Worker.Domain;
using Saturn.Worker.Domain.Test;
using Saturn.Core.API.Test;
using Saturn.Core.API;
using Castle.MicroKernel.Registration;
using NUnit.Framework;

namespace Saturn.Worker.Capture
{
    [TestFixture]
    public class DetailTransformerTest : AbstractDITest
    {
        public DetailTransformerTest() : base(new IWindsorInstaller[] { new ServiceInstaller(), new WorkerUnitTestInstaller(), new CaptureWorkerInstaller() }) { }

        [Test]
        public void TestTransform()
        {
            IMockFactory<Batch> mockFactory = this.GetContainer().Resolve<IMockFactory<Batch>>();
            Batch batch = mockFactory.GetMock();

            IBatchDetailTransformer<string> transformer = this.GetContainer().Resolve<IBatchDetailTransformer<string>>();
            string transformation = transformer.Transform(batch, batch.Details[0]);
            Assert.IsFalse(String.IsNullOrEmpty(transformation));
        }
    }
}
