﻿using System;
using Saturn.Worker.Data.Binary;
using Saturn.Worker.Domain;
using Saturn.Worker.Domain.Test;
using Saturn.Core.API;
using System.IO;
using System.Collections.Generic;
using Saturn.Core.API.Test;
using Castle.MicroKernel.Registration;
using NUnit.Framework;

namespace Saturn.Worker.Capture
{
    [TestFixture]
    public class BatchClaimCheckerTest : AbstractDITest
    {

        public BatchClaimCheckerTest() : base( new IWindsorInstaller[]{new WorkerUnitTestInstaller()}) { }

        [Test]
        public void TestIsValid()
        {
            IWorkSummaryRepository repository = this.GetContainer().Resolve<IWorkSummaryRepository>();
            Assert.IsTrue(repository is FileWorkSummaryRepository);

            DateTime dateTime = TimeProvider.Current.UtcNow;
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string workingDir = Path.Combine(((FileWorkSummaryRepository)repository).GetWorkingDirectory(), year, month, day);

            DirectoryInfo dirInfo = CleanDirectory(workingDir);
            Assert.IsTrue(dirInfo.GetFiles().Length == 0);

            string testString = "TESTE";
            IMockFactory<WorkSummary> mockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            repository.Store(mockFactory.GetMock(testString));

            Assert.IsTrue(dirInfo.GetFiles().Length > 0);

            IClaimChecker<Batch> claimChecker = new BatchClaimChecker(repository);

            Batch validBatch = new Batch();
            validBatch.BatchId = "VALID_TERMINAL_ID";

            Batch invalidBatch = new Batch();
            invalidBatch.BatchId = testString + "0";

            Assert.IsTrue(claimChecker.IsValid(validBatch));
            Assert.IsFalse(claimChecker.IsValid(invalidBatch));
        }
    }
}
