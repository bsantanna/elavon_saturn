﻿using Saturn.Core.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;

namespace Saturn.Worker.Capture.Test
{
    public class MockRandomBreakingHttpService : IHttpService
    {
        private int requestCount = 0;

        public HttpResult Post(Uri uri, Stream stream, string data)
        {
            requestCount++;
            if (requestCount % 2 == 0)
            {
                throw new IOException("Fake IOException simulating connection error");
            }
            else
            {
                HttpResult result = new HttpResult();
                result.StartTime = TimeProvider.Current.UtcNow;
                result.Request = "MOCK_HTTP_SERVICE" + Environment.NewLine + data;
                if (data.IndexOf("HD.Network_Status_Byte=%23") >= 0)
                {
                    result.Response = "9F.Status=A";
                }
                else
                {
                    result.Response = "89.Response_Message=FECHA";
                }
                result.EndTime = TimeProvider.Current.UtcNow;
                return result;
            }
        }

        public TcpClient GetTcpClient(Uri uri)
        {
            return new TcpClient();
        }

        public Stream OpenStream(Uri uri, TcpClient tcpClient)
        {
            return null;
        }
    }
}
