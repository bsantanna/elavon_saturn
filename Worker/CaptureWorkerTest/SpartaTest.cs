﻿using System;
using Saturn.Worker.Domain.Test;
using Castle.MicroKernel.Registration;
using Saturn.Core.API;
using Saturn.Worker.Common;
using Saturn.Worker.Error;
using Saturn.Worker.Logs;
using Saturn.Worker.Feed;
using Saturn.Worker.Capture.Logs;
using Saturn.Worker.Capture;
using Saturn.Worker.Domain;
using System.IO;
using Saturn.Worker.Data.Binary;
using Saturn.Core.API.Test;
using NUnit.Framework;

namespace Saturn.Worker.Capture
{
    [TestFixture]
    public class SpartaTest : AbstractWorkerTest
    {

        public SpartaTest()
            : base(
                new IWindsorInstaller[] {
                new WorkerUnitTestInstaller(),
                new ServiceInstaller(),
                new CircuitBreakerInstaller(),
                new WorkerCommonInstaller(),
                new WorkerErrorHandlingInstaller(),
                new WorkerLogsInstaller(),
                new WorkerFeedInstaller(),
                new CaptureLogsInstaller(),
                new CaptureWorkerInstaller(), 
                new CaptureWorkerRuntimeInstaller()}) { }

        [Test]
        public void Test300Transactions()
        {
            var container = GetContainer();
            WorkSummary workSummary = new WorkSummary(container.Resolve<IWorkerContext>());
            workSummary.StartTime = TimeProvider.Current.UtcNow;
            IBatchRepository repository = container.Resolve<IBatchRepository>();
            foreach (Batch batch in repository.GetBatches())
            {
                workSummary.PendingBatches.Enqueue(batch);
            }
            IWorker worker = GetWorker<CaptureWorker>();
            worker.Work(workSummary);
        }
    }
}