﻿using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using NUnit.Framework;
using Saturn.Core.API;
using Saturn.Core.API.Test;
using Saturn.Worker.Capture.Logs;
using Saturn.Worker.Capture.Test;
using Saturn.Worker.Common;
using Saturn.Worker.Data.Binary;
using Saturn.Worker.Domain;
using Saturn.Worker.Domain.Test;
using Saturn.Worker.Error;
using Saturn.Worker.Feed;
using Saturn.Worker.Logs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Saturn.Worker.Capture
{
    [TestFixture]
    public class CaptureWorkerTest : AbstractWorkerTest
    {

        public CaptureWorkerTest()
            : base(new IWindsorInstaller[] { new WorkerUnitTestInstaller(), new ServiceInstaller(), new CircuitBreakerInstaller(), new WorkerCommonInstaller(), new WorkerErrorHandlingInstaller(), new WorkerLogsInstaller(), new WorkerFeedInstaller(), new CaptureLogsInstaller(), new CaptureWorkerInstaller() }) { }

        [Test]
        public void TestDepedencyResolve()
        {
            RegisterHttpService(typeof(MockHttpService));
            var container = GetContainer();
            IMockFactory<Batch> batchMockFactory = container.Resolve<IMockFactory<Batch>>();
            IBatchRepository batchRepository = container.Resolve<IBatchRepository>();
            IScheduleRepository scheduleRepository = container.Resolve<IScheduleRepository>();
            IWorkerContext workerContext = container.Resolve<IWorkerContext>();
            IBatchTransformer<string> batchTransformer = container.Resolve<IBatchTransformer<string>>();
            IClaimChecker<Batch> batchClaimChecker = container.Resolve<IClaimChecker<Batch>>();
            IHttpService httpService = container.Resolve<IHttpService>();
            IMailService mailService = container.Resolve<IMailService>();
            ICaptureService captureService = container.Resolve<ICaptureService>();
            IWorker worker = GetWorker<CaptureWorker>();
            Assert.IsNotNull(worker);
        }


        [Test]
        public void TestRebuildDetails()
        {
            RegisterHttpService(typeof(MockHttpService));
            IWorker worker = GetWorker<CaptureWorker>();
            IMockFactory<BatchDetail> batchMockFactory = GetContainer().Resolve<IMockFactory<BatchDetail>>();
            ICollection<BatchDetail> fakeProcessedDetails = new HashSet<BatchDetail> { };
            IList<BatchDetail> fakeAllDetails = new List<BatchDetail>();
            for (int i = 0; i < 5; i++)
            {
                fakeAllDetails.Add(batchMockFactory.GetMock(i));
            }

            BatchDetail errorDetail = null;
            // suppose detail 4 got an error
            for (int i = 0; i < 4; i++)
            {
                if (i < 3)
                {
                    fakeProcessedDetails.Add(fakeAllDetails[i]);
                }
                else
                {
                    errorDetail = fakeAllDetails[i];
                }
            }

            var proxy = worker as IProxyTargetAccessor;
            CaptureWorker captureWorker = null;
            if (proxy != null)
            {
                captureWorker = (CaptureWorker)proxy.DynProxyGetTarget();
            }
            else
            {
                captureWorker = (CaptureWorker)worker;
            }

            IList<BatchDetail> rebuildDetails = captureWorker.RebuildDetails(fakeProcessedDetails, fakeAllDetails);
            Assert.IsFalse(rebuildDetails.Contains(errorDetail));
            Assert.IsTrue(rebuildDetails.Count < fakeAllDetails.Count);
            Assert.IsTrue(rebuildDetails[3].GetHashCode().Equals(fakeAllDetails[4].GetHashCode()));
        }

        [Test]
        public void TestWork()
        {
            RegisterHttpService(typeof(MockHttpService));

            IMockFactory<WorkSummary> mockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            WorkSummary workSummary = mockFactory.GetMock();

            IWorker worker = GetWorker<CaptureWorker>();
            worker.Work(workSummary);
            Assert.IsTrue(workSummary.ProcessedBatches.Count > 0);
        }

        [Test]
        public void TestHttpConnectionError()
        {
            RegisterHttpService(typeof(MockRandomBreakingHttpService));

            IWorkSummaryRepository repository = this.GetContainer().Resolve<IWorkSummaryRepository>();
            Assert.IsTrue(repository is FileWorkSummaryRepository);

            DateTime dateTime = TimeProvider.Current.UtcNow;
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string workingDir = Path.Combine(((FileWorkSummaryRepository)repository).GetWorkingDirectory(), year, month, day);

            DirectoryInfo dirInfo = CleanDirectory(((FileWorkSummaryRepository)repository).GetWorkingDirectory());
            Assert.IsTrue(dirInfo.GetFiles().Length == 0);

            string testString = "TEST_HTTP_ERROR_PENDING";

            IMockFactory<WorkSummary> mockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            WorkSummary workSummary = mockFactory.GetMock(testString);

            IWorker worker = GetWorker<CaptureWorker>();
            worker.Work(workSummary);
            Assert.IsTrue(dirInfo.GetFiles().Length > 0);

            IEnumerable<WorkSummary> pendingWork = repository.GetPendingWork();
            foreach (var work in pendingWork)
            {
                int counter = 0;
                foreach (var batch in work.ProcessedBatches)
                {
                    Assert.AreEqual(testString + counter, batch.BatchId);
                    counter++;
                }
            }
        }

        [Test]
        public void TestRecoverPendingWork()
        {
            RegisterHttpService(typeof(MockBreakOnceHttpService));

            IWorkSummaryRepository repository = this.GetContainer().Resolve<IWorkSummaryRepository>();
            Assert.IsTrue(repository is FileWorkSummaryRepository);

            DateTime dateTime = TimeProvider.Current.UtcNow;
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string workingDir = Path.Combine(((FileWorkSummaryRepository)repository).GetWorkingDirectory(), year, month, day);

            DirectoryInfo dirInfo = CleanDirectory(((FileWorkSummaryRepository)repository).GetWorkingDirectory());
            Assert.IsTrue(dirInfo.GetFiles().Length == 0);

            string testString = "TEST_HTTP_ERROR_PENDING_SUCCESS";

            IMockFactory<WorkSummary> mockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            WorkSummary workSummary = mockFactory.GetMock(testString);
            workSummary.ProcessedBatches.Clear();

            IWorker worker = GetWorker<CaptureWorker>();
            worker.Work(workSummary);
            Assert.IsTrue(dirInfo.GetFiles().Length > 0);

            IEnumerable<WorkSummary> pendingWork = repository.GetPendingWork();

            // test before circuit breaker enter in half-open state
            foreach (var work in pendingWork)
            {
                Assert.IsTrue(work.PendingBatches.Count > 0);
                Assert.IsTrue(work.ProcessedBatches.Count == 0);

                worker.Work(work);

                Assert.IsTrue(work.PendingBatches.Count > 0);
                Assert.IsTrue(work.ProcessedBatches.Count == 0);

            }

            // test circuit breaker recovery
            Thread.Sleep(31 * 1000);

            pendingWork = repository.GetPendingWork();
            foreach (var work in pendingWork)
            {
                Assert.IsTrue(work.PendingBatches.Count > 0);
                Assert.IsTrue(work.ProcessedBatches.Count == 0);

                worker.Work(work);

                Assert.IsTrue(work.PendingBatches.Count == 0);
                Assert.IsTrue(work.ProcessedBatches.Count > 0);
            }

        }
    }
}
