﻿using System;
using Saturn.Worker.Domain;
using Saturn.Worker.Domain.Test;
using Saturn.Core.API.Test;
using NUnit.Framework;

namespace Saturn.Worker.Capture
{
    [TestFixture]
    public class TrailerTransformerTest
    {
        [Test]
        public void TestTransform()
        {
            IMockFactory<BatchDetail> batchDetailMockFactory = new BatchDetailMockFactory();
            IMockFactory<Batch> batchMockFactory = new BatchMockFactory(batchDetailMockFactory);
            IBatchTransformer<string> transformer = new TrailerTransformer();
            string transformation = transformer.Transform(batchMockFactory.GetMock());
            Assert.IsFalse(String.IsNullOrEmpty(transformation));
        }
    }
}
