﻿using Saturn.Core.API.Test;
using Saturn.Worker.Domain;
using Saturn.Worker.Domain.Test;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Data.Test
{
    public class MockBatchRepository : IBatchRepository
    {
        private bool alreadyCalled = false;

        public ICollection<Batch> GetBatches()
        {
            ICollection<Batch> batches = new HashSet<Batch>();
            if (!alreadyCalled)
            {
                IMockFactory<BatchDetail> batchDetailMockFactory = new BatchDetailMockFactory();
                IMockFactory<Batch> batchMockFactory = new BatchMockFactory(batchDetailMockFactory);
                batches.Add(batchMockFactory.GetMock());
                alreadyCalled = true;
            }
            return batches;
        }
    }
}
