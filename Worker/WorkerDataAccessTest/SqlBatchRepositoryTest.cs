﻿using System;
using Saturn.Core.API;
using Saturn.Worker.Domain.Test;
using Saturn.Worker.Domain;
using System.Collections.Generic;
using NUnit.Framework;

namespace Saturn.Worker.Data.Sql
{
    [TestFixture]
    public class SqlBatchRepositoryTest
    {
        [Test]
        public void TestGetBatches()
        {
            IXmlService xmlService = new XmlService();
            IWorkerContext workerContext = new UnitTestWorkerContext() as IWorkerContext;
            IBatchRepository batchRepository = new SqlBatchRepository(workerContext, xmlService);
            Assert.IsNotNull(batchRepository);

            ICollection<Batch> batches = batchRepository.GetBatches();
            Assert.IsNotNull(batches);
        }
    }
}
