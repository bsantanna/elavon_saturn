﻿using System;
using System.Collections.Generic;
using Saturn.Worker.Domain.Test;
using Saturn.Worker.Domain;
using System.IO;
using Saturn.Core.API;
using Saturn.Core.API.Test;
using Castle.MicroKernel.Registration;
using NUnit.Framework;

namespace Saturn.Worker.Data.Binary
{
    [TestFixture]
    public class FileWorkSummaryRepositoryTest : AbstractDITest
    {
        public FileWorkSummaryRepositoryTest() : base(new IWindsorInstaller[] { new WorkerUnitTestInstaller() }) { }

        [Test]
        public void TestGetPendingWork()
        {
            IWorkSummaryRepository repository = this.GetContainer().Resolve<IWorkSummaryRepository>();
            Assert.IsTrue(repository is FileWorkSummaryRepository);

            DateTime dateTime = TimeProvider.Current.UtcNow;
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string workingDir = Path.Combine(((FileWorkSummaryRepository)repository).GetWorkingDirectory(), year, month, day);

            DirectoryInfo dirInfo = CleanDirectory(workingDir);
            Assert.IsTrue(dirInfo.GetFiles().Length == 0);

            IMockFactory<WorkSummary> workSummaryMockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            WorkSummary workSummary = workSummaryMockFactory.GetMock();

            string testString = "TEST_PENDING";
            IMockFactory<Batch> batchMockFactory = this.GetContainer().Resolve<IMockFactory<Batch>>();
            workSummary.PendingBatches.Enqueue(batchMockFactory.GetMock(testString));
            repository.Store(workSummary);

            Assert.IsTrue(dirInfo.GetFiles().Length > 0);

            IEnumerable<WorkSummary> pendingWork = repository.GetPendingWork();

            foreach (var work in pendingWork)
            {
                foreach (Batch batch in work.PendingBatches)
                {
                    Assert.IsTrue(testString.Equals(batch.BatchId));
                }
            }

            Assert.IsTrue(dirInfo.GetFiles().Length == 0);
        }

        [Test]
        public void TestStoreWorkSummary()
        {
            IWorkSummaryRepository repository = this.GetContainer().Resolve<IWorkSummaryRepository>();
            Assert.IsTrue(repository is FileWorkSummaryRepository);

            DateTime dateTime = TimeProvider.Current.UtcNow;
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string workingDir = Path.Combine(((FileWorkSummaryRepository)repository).GetWorkingDirectory(), year, month, day);

            DirectoryInfo dirInfo = CleanDirectory(workingDir);
            Assert.IsTrue(dirInfo.GetFiles().Length == 0);

            IMockFactory<WorkSummary> mockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            repository.Store(mockFactory.GetMock());

            Assert.IsTrue(dirInfo.GetFiles().Length > 0);
        }

        [Test]
        public void TestGetWorkSummary()
        {
            IWorkSummaryRepository repository = this.GetContainer().Resolve<IWorkSummaryRepository>();
            Assert.IsTrue(repository is FileWorkSummaryRepository);

            DateTime dateTime = TimeProvider.Current.UtcNow;
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string workingDir = Path.Combine(((FileWorkSummaryRepository)repository).GetWorkingDirectory(), year, month, day);

            DirectoryInfo dirInfo = CleanDirectory(workingDir);

            string testString = "TESTE";
            IMockFactory<WorkSummary> mockFactory = this.GetContainer().Resolve<IMockFactory<WorkSummary>>();
            repository.Store(mockFactory.GetMock(testString));

            IEnumerable<WorkSummary> workSummarySet = repository.GetAllFromDate(TimeProvider.Current.UtcNow);
            bool empty = true;
            foreach (WorkSummary workSummary in workSummarySet)
            {
                empty = false;
                int counter = 0;
                foreach (Batch batch in workSummary.ProcessedBatches)
                {
                    Assert.AreEqual(testString + counter, batch.BatchId);
                    Assert.IsNotNull(batch.HeaderHttpResult.StartTime);
                    Assert.IsTrue(TimeProvider.Current.UtcNow > batch.HeaderHttpResult.StartTime);
                    counter++;
                }
            }
            Assert.IsFalse(empty);
        }
    }
}
