﻿using Saturn.Core.API.Test;
using Saturn.Worker.Domain;
using Saturn.Worker.Domain.Test;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Data.Test
{
    public class MockScheduleRepository : IScheduleRepository
    {
        public Schedule GetSchedule()
        {
            IMockFactory<Schedule> scheduleMockFactory = new ScheduleMockFactory();
            return scheduleMockFactory.GetMock();
        }

        public void UpdateSchedule(Schedule schedule)
        {
            // do nothing
        }
    }
}
