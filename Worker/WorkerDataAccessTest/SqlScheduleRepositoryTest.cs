﻿using Saturn.Worker.Domain;
using Saturn.Worker.Capture;
using NUnit.Framework;

namespace Saturn.Worker.Data.Sql
{
    [TestFixture]
    public class SqlScheduleRepositoryTest
    {
        [Test]
        public void TestGetSchedule()
        {
            IWorkerContext workerContext = new CaptureWorkerContext() as IWorkerContext;
            IScheduleRepository scheduleRepository = new SqlScheduleRepository(workerContext);
            Assert.IsNotNull(scheduleRepository);

            Schedule schedule = scheduleRepository.GetSchedule();
            Assert.IsNotNull(schedule);
        }
    }
}
