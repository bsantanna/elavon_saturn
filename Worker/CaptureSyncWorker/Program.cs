﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net;
using log4net.Config;
using Saturn.Core.API;
using Saturn.Worker.Capture.Sync;
using Saturn.Worker.Common;
using Saturn.Worker.Error;
using Saturn.Worker.Feed;
using Saturn.Worker.Logs;

namespace Saturn.Worker.Capture.Sync
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            // Configure log4net using App.Config
            XmlConfigurator.Configure();
            log.Debug("Logging engine configured.");

            // initialize DI container with installers
            IWindsorInstaller[] installers = new IWindsorInstaller[] {
                 new ServiceInstaller(),
                 new CircuitBreakerInstaller(),
                 new WorkerCommonInstaller(),
                 new WorkerErrorHandlingInstaller(),
                 new WorkerLogsInstaller(),
                 new WorkerFeedInstaller(),
                 new CaptureSyncWorkerInstaller()
            };

            log.Info("Starting DI Container...");
            WindsorContainer container = new WindsorContainer();
            container.Install(installers);

            log.Info("Resolving Workers...");
            System.ServiceProcess.ServiceBase[] workers = container.ResolveAll<System.ServiceProcess.ServiceBase>();

            log.Info("Executing Windows Service...");
            System.ServiceProcess.ServiceBase.Run(workers);

            foreach (System.ServiceProcess.ServiceBase worker in workers)
            {
                container.Release(worker);
            }
        }
    }
}
