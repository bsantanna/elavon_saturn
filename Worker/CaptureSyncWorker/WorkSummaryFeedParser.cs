﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Common;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;

namespace Saturn.Worker.Capture.Sync
{
    public class WorkSummaryFeedParser : AbstractWorkSummaryFeedParser
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IBackOfficeRepository backOfficeRepository;
        private readonly ICollection<BatchDetail> validDetails;
        private readonly ICollection<BatchDetail> invalidDetails;
        private readonly IHttpResultValidator batchSegmentHttpResultValidator;
        private readonly IHttpResultValidator trailerHttpResultValidator;

        public WorkSummaryFeedParser(IWorkerContext workerContext, IBackOfficeRepository backOfficeRepository, IHttpResultValidator batchSegmentHttpResultValidator, IHttpResultValidator trailerHttpResultValidator)
            : base(workerContext)
        {
            log.Debug("Creating instance...");

            if (backOfficeRepository == null)
            {
                throw new ArgumentNullException("backOfficeRepository");
            }
            this.backOfficeRepository = backOfficeRepository;

            if (batchSegmentHttpResultValidator == null)
            {
                throw new ArgumentNullException("batchSegmentHttpResultValidator");
            }
            this.batchSegmentHttpResultValidator = batchSegmentHttpResultValidator;

            if (trailerHttpResultValidator == null)
            {
                throw new ArgumentNullException("trailerHttpResultValidator");
            }
            this.trailerHttpResultValidator = trailerHttpResultValidator;

            // class scoped vars
            this.validDetails = new List<BatchDetail>();
            this.invalidDetails = new List<BatchDetail>();
        }

        private string GetTruncateResponse(string response, int size)
        {
            return response.Length <= size ? response : response.Substring(0, size);
        }

        private string GetFormattedRequest(HttpResult httpResult)
        {
            return Regex.Split(httpResult.Request, "\n").Last().Replace("&", Environment.NewLine);
        }

        protected override void ProcessBatchDetail(BatchDetail batchDetail, bool batchDetailIsValid)
        {
            // add to local collections for further processing
            if (batchDetailIsValid)
            {
                this.validDetails.Add(batchDetail);
            }
            else
            {
                this.invalidDetails.Add(batchDetail);
            }
        }

        protected override void ProcessBatchDetailFeed(SyndicationFeed batchDetailFeed, bool batchIsValid)
        {
            Batch batch = batchDetailFeed.ElementExtensions.ReadElementExtensions<Batch>("Batch", "http://schemas.elavon.com.br").Single();

            if (!batchIsValid)
            {
                log.Warn("Batch[" + batch.BatchId + "] is marked as invalid, will not update backoffice for this one.");
                this.validDetails.Clear();
                this.invalidDetails.Clear();
            }
            else
            {
                string paymentTransaction = "DoPaymentCapture";
                string notOkStatus = "NOTOK";
                string source = "Saturn_Worker_" + workerContext.WorkerName() + "<=>ViaConex";

                log.Debug("Updating Header in BackOffice for Batch => " + batch.BatchId);
                HttpResult httpResult = batch.HeaderHttpResult;
                backOfficeRepository.UpdateHeader(batch.BatchId, batch.TerminalId, GetFormattedRequest(httpResult), httpResult.Response);

                if (!batchSegmentHttpResultValidator.IsValid(httpResult))
                {
                    backOfficeRepository.LogTasker(batch.TerminalId, notOkStatus, paymentTransaction, httpResult.Response, (validDetails.Count + 2).ToString(), httpResult.StartTime);
                }

                if (invalidDetails.Count > 0)
                {
                    log.Debug("Updating " + invalidDetails.Count + " invalid details for Batch => " + batch.BatchId);
                    foreach (BatchDetail batchDetail in invalidDetails)
                    {
                        httpResult = batchDetail.DetailHttpResult;

                        backOfficeRepository.LogTasker(batch.TerminalId, batchDetail.TransactionID, notOkStatus, paymentTransaction, httpResult.Response, (validDetails.Count + 2).ToString(), httpResult.StartTime);

                        IDictionary<string, string> responseDictionary = GetResponseDictionary(httpResult);
                        string responseCode = responseDictionary.ContainsKey("9F.Status") ? responseDictionary["9F.Status"] : BusinessConstants.GetDescriptionValue(BusinessConstants.ResponseCode.SystemUnavailable);
                        string responseMessage = responseDictionary.ContainsKey("9F.Results") ? responseDictionary["9F.Results"] : GetTruncateResponse(httpResult.Response, 255);
                        backOfficeRepository.LogTrans(batch.TerminalId, batchDetail.TransactionID, paymentTransaction, source, responseCode, responseMessage, GetFormattedRequest(httpResult), httpResult.Response, batch.ProcessorHostName, httpResult.StartTime, httpResult.EndTime);
                    }
                    invalidDetails.Clear();
                }

                log.Debug("Updating " + validDetails.Count + " valid details for Batch => " + batch.BatchId);
                foreach (BatchDetail batchDetail in validDetails)
                {
                    httpResult = batchDetail.DetailHttpResult;
                    backOfficeRepository.UpdateDetail(batch.BatchId, batch.TerminalId, batchDetail.TransactionID, GetFormattedRequest(httpResult), httpResult.Response);

                    IDictionary<string, string> responseDictionary = GetResponseDictionary(httpResult);
                    string responseCode = responseDictionary.ContainsKey("9F.Status") ? responseDictionary["9F.Status"] : BusinessConstants.GetDescriptionValue(BusinessConstants.ResponseCode.SystemUnavailable);
                    string responseMessage = responseDictionary.ContainsKey("9F.Results") ? responseDictionary["9F.Results"] : GetTruncateResponse(httpResult.Response, 255);
                    backOfficeRepository.LogTrans(batch.TerminalId, batchDetail.TransactionID, paymentTransaction, source, responseCode, responseMessage, GetFormattedRequest(httpResult), httpResult.Response, batch.ProcessorHostName, httpResult.StartTime, httpResult.EndTime);

                }
                validDetails.Clear();

                log.Debug("Updating Trailer in BackOffice for Batch => " + batch.BatchId);

                httpResult = batch.TrailerHttpResult;
                backOfficeRepository.UpdateTrailer(batch.BatchId, batch.TerminalId, GetFormattedRequest(httpResult), httpResult.Response);

                if (!trailerHttpResultValidator.IsValid(httpResult))
                {
                    backOfficeRepository.LogTasker(batch.TerminalId, notOkStatus, paymentTransaction, httpResult.Response, (validDetails.Count + 2).ToString(), httpResult.StartTime);
                }
            }
        }

        protected override void ProcessBatchFeed(SyndicationFeed batchFeed)
        {
            log.Debug("Finished processing feed => " + batchFeed.Id);
        }
    }
}
