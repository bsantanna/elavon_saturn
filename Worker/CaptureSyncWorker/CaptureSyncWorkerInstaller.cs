﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Saturn.Core.API;
using Saturn.Worker.Common;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed;
using System.ServiceProcess;

namespace Saturn.Worker.Capture.Sync
{
    public class CaptureSyncWorkerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IWorkerContext>()
                    .ImplementedBy<CaptureSyncWorkerContext>()
            );

            container.Register(
               Component
                   .For<IHttpResultValidator>()
                   .ImplementedBy<BatchSegmentHttpResultValidator>()
            );

            container.Register(
                Component
                    .For<IHttpResultValidator>()
                    .ImplementedBy<TrailerHttpResultValidator>()
            );

            container.Register(
                Component
                    .For<IFeedParser>()
                    .ImplementedBy<WorkSummaryFeedParser>()
                    .DependsOn(new Dependency[] { 
                        Dependency.OnComponent("trailerHttpResultValidator", typeof(TrailerHttpResultValidator))
                    })
            );

            container.Register(
               Component
                   .For<ServiceBase>()
                   .ImplementedBy<CaptureSyncWorker>()
                   .DependsOn(new Dependency[] { 
                        Dependency.OnComponent("terminateWorker", typeof(TerminateWorker))
                    })
           );
        }
    }
}
