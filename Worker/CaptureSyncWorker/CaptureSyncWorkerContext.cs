﻿using log4net;
using Saturn.Worker.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Capture.Sync
{
    public class CaptureSyncWorkerContext : AbstractConfigurationWorkerContext
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CaptureSyncWorkerContext():base()
        {
            log.Debug("Creating instance...");
        }
        
        public override string WorkerName()
        {
            return "CaptureSync";
        }
    }
}
