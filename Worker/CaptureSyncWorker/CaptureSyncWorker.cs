﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed;
using System;

namespace Saturn.Worker.Capture.Sync
{
    public class CaptureSyncWorker : AbstractWorker
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IFeedParser workSummaryFeedParser;

        public CaptureSyncWorker(IWorkerContext workerContext, IMailService mailService, IBatchService batchService, IWorker terminateWorker, IFeedParser workSummaryFeedParser)
            : base(workerContext, mailService, batchService, terminateWorker)
        {
            log.Debug("Creating instance...");

            if (workSummaryFeedParser == null)
            {
                throw new ArgumentNullException("workSummaryFeedParser");
            }
            this.workSummaryFeedParser = workSummaryFeedParser;
        }

        public override void Work(WorkSummary workSummary)
        {
            Uri feedUri = workSummaryFeedParser.GetFeedUri();
            log.Debug("Parsing feed from URI: " + feedUri.ToString());
            workSummaryFeedParser.ProcessFeed(feedUri);
        }

        protected override void OnStop()
        {
            this.workInProgress = false;
            base.OnStop();
        }

        protected override bool ShouldExecute()
        {
            // always execute
            return true;
        }
    }
}
