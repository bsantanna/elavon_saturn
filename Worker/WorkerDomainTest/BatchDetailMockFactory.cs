﻿using Saturn.Core.API;
using Saturn.Core.API.Test;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain.Test
{
    public class BatchDetailMockFactory : IMockFactory<BatchDetail>
    {
        public BatchDetail GetMock()
        {
            BatchDetail batchDetail = new BatchDetail();
            batchDetail.Amount = "100";
            batchDetail.Request = "Credit Card.Sale";
            batchDetail.PurchaseDepartureDate = "141023";
            batchDetail.AuthorizationSource = "";
            batchDetail.PS2000Data = "W1131449677537311029K";
            batchDetail.MSDI = "1";
            batchDetail.MerchantDynamicDBA = "ELAVON*LOJA ecommerce";
            batchDetail.PurchaseAdditionalID = "EcomCredReg";
            batchDetail.CardProduct = "Visa.Credit";
            batchDetail.ECI = "7";
            batchDetail.CardData = "4000000000000010=1230";
            batchDetail.AuthAmount = "100";
            batchDetail.AuthAmountCurrencyCode = "BRL";
            batchDetail.POSEntryCapability = "01";
            batchDetail.CardEntryMode = "01";
            batchDetail.NSUHost = "2362202221";
            batchDetail.CaptureDetailsAmount = "5000";
            batchDetail.ApprovalCode = "QVI312";
            batchDetail.AuthorizationDateTime = "2014-10-23T17:16:42-02:00";
            batchDetail.CVV2Response = "M";
            batchDetail.TransactionID = TimeProvider.Current.UtcNow.Millisecond.ToString();

            return batchDetail;
        }


        public BatchDetail GetMock(object uniqueIdentifier)
        {
            BatchDetail batchDetail = GetMock();
            batchDetail.TransactionID += uniqueIdentifier.ToString();
            return batchDetail;
        }
    }
}