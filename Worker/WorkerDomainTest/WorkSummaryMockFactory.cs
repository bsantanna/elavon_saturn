﻿using Saturn.Core.API;
using Saturn.Core.API.Test;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Domain.Test
{
    public class WorkSummaryMockFactory : IMockFactory<WorkSummary>
    {
        private readonly IWorkerContext workerContext;

        private readonly IMockFactory<Batch> batchMockFactory;

        public WorkSummaryMockFactory(IWorkerContext workerContext, IMockFactory<Batch> batchMockFactory)
        {
            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;

            if (batchMockFactory == null)
            {
                throw new ArgumentNullException("batchMockFactory");
            }
            this.batchMockFactory = batchMockFactory;
        }

        public WorkSummary GetMock()
        {
            return GetMock("test");
        }

        public WorkSummary GetMock(object uniqueIdentifier)
        {
            WorkSummary workSummary = new WorkSummary(this.workerContext);
            workSummary.StartTime = TimeProvider.Current.UtcNow;

            for (int i = 0; i < 10; i++)
            {
                Batch batch = batchMockFactory.GetMock(uniqueIdentifier + i.ToString());

                HttpResult sampleHeaderHttpResult = new HttpResult();
                sampleHeaderHttpResult.StartTime = TimeProvider.Current.UtcNow;
                sampleHeaderHttpResult.Request = "Mock Header REQUEST " + i;
                sampleHeaderHttpResult.Response = "Mock Header RESPONSE " + i;
                batch.HeaderHttpResult = sampleHeaderHttpResult;

                HttpResult sampleTrailerHttpResult = new HttpResult();
                sampleTrailerHttpResult.StartTime = TimeProvider.Current.UtcNow;
                sampleTrailerHttpResult.Request = "Mock Trailer REQUEST " + i;
                sampleTrailerHttpResult.Response = "Mock Trailer RESPONSE " + i;
                batch.TrailerHttpResult = sampleTrailerHttpResult;

                workSummary.StartTime = TimeProvider.Current.UtcNow;
                workSummary.EndTime = TimeProvider.Current.UtcNow;
                workSummary.ProcessedBatches.Add(batch);
            }

            return workSummary;
        }
    }
}
