﻿using System;
using Saturn.Core.API;
using NUnit.Framework;

namespace Saturn.Worker.Domain
{
    [TestFixture]
    public class BatchSegmentHttpResultValidatorTest
    {
        [Test]
        public void TestIsValid()
        {
            HttpResult validResult = new HttpResult();
            validResult.Response = "9F.Status=A";

            HttpResult invalidResult = new HttpResult();
            invalidResult.Response = "blablabla";

            IHttpResultValidator validator = new BatchSegmentHttpResultValidator();
            Assert.IsTrue(validator.IsValid(validResult));
            Assert.IsFalse(validator.IsValid(invalidResult));
        }
    }
}
