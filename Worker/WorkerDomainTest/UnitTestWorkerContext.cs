﻿using Saturn.Worker.Common;
using System;

namespace Saturn.Worker.Domain.Test
{
    public class UnitTestWorkerContext : AbstractConfigurationWorkerContext
    {
        public override string WorkerName()
        {
            return "Test";
        }
    }
}
