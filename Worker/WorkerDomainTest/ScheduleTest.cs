﻿using System;
using Saturn.Core.API;
using NUnit.Framework;

namespace Saturn.Worker.Domain
{
    [TestFixture]
    public class ScheduleTest
    {
        [Test]
        public void TestIsOnTime()
        {
            int currentMinute = TimeProvider.Current.UtcNow.Minute;
            int currentHour = TimeProvider.Current.UtcNow.Hour;

            Schedule schedule = new Schedule();
            schedule.MinuteToRun = currentMinute.ToString();
            schedule.HourToRun = currentHour.ToString();

            Assert.IsTrue(schedule.IsOnTime());

            schedule.MinuteToRun = (currentMinute - 1).ToString();
            Assert.IsFalse(schedule.IsOnTime());
        }
    }
}
