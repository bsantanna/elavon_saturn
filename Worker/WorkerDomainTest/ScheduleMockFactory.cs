﻿using Saturn.Core.API;
using Saturn.Core.API.Test;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain.Test
{
    public class ScheduleMockFactory : IMockFactory<Schedule>
    {
        public Schedule GetMock()
        {
            int currentMinute = TimeProvider.Current.UtcNow.Minute;
            int currentHour = TimeProvider.Current.UtcNow.Hour;

            Schedule schedule = new Schedule();
            schedule.MinuteToRun = currentMinute.ToString();
            schedule.HourToRun = currentHour.ToString();

            return schedule;
        }

        public Schedule GetMock(object uniqueIdentifier)
        {
            Schedule schedule = GetMock();
            schedule.MinuteToRun = uniqueIdentifier.ToString();
            return schedule;
        }
    }
}