﻿using NUnit.Framework;
using System;

namespace Saturn.Worker.Domain
{
    [TestFixture]
    public class BatchTest
    {
        [Test]
        public void TestGetTotalAmount()
        {
            int A = 11;
            int B = 22;
            Batch batch = new Batch();
            BatchDetail batchDetail = new BatchDetail();
            batchDetail.Amount =  A.ToString();
            batch.Details.Add(batchDetail);
            batchDetail = new BatchDetail();
            batchDetail.Amount = B.ToString();
            batch.Details.Add(batchDetail);

            Assert.AreEqual(A + B, batch.GetTotalAmount());
        }
    }
}
