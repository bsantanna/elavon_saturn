﻿using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Saturn.Core.API.Test;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;

namespace Saturn.Worker.Domain.Test
{
    public abstract class AbstractWorkerTest : AbstractDITest
    {
        public AbstractWorkerTest(IWindsorInstaller[] installers) : base(installers) {
            // clean working directory
            IWorkerContext workerContext = GetContainer().Resolve<IWorkerContext>();
            if (workerContext != null)
            {
                string workingDir = Path.Combine(workerContext.AppDataDirectory(), "Saturn_Worker", workerContext.WorkerName());
                CleanDirectory(workingDir);
            }
        }

        protected IWorker GetWorker<T>()
        {
            var container = GetContainer();
            ServiceBase[] workers = container.ResolveAll<ServiceBase>();
            foreach (IWorker worker in workers)
            {
                var proxy = worker as IProxyTargetAccessor;
                if (proxy != null && proxy.DynProxyGetTarget() is T)
                {
                    return worker;
                }
                else if (worker is T)
                {
                    return worker;
                }
            }
            return null;
        }
    }
}
