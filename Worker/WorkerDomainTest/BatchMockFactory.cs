﻿using Saturn.Core.API;
using Saturn.Core.API.Test;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain.Test
{
    public class BatchMockFactory : IMockFactory<Batch>
    {

        private IMockFactory<BatchDetail> batchDetailMockFactory;

        public BatchMockFactory(IMockFactory<BatchDetail> batchDetailMockFactory)
        {
            this.batchDetailMockFactory = batchDetailMockFactory;
        }

        public Batch GetMock()
        {
            Batch batch = new Batch();
            batch.BatchId = "MOCK" + TimeProvider.Current.UtcNow.Millisecond.ToString();
            batch.TerminalId = "4012050000000000000045";
            batch.Version = "4013";
            batch.ApplicationId = "TZ9999IC";
            batch.BatchNumber = "000";

            for (int i = 0; i < 300; i++)
            {
                BatchDetail batchDetail = batchDetailMockFactory.GetMock(i);

                HttpResult sampleHttpResult = new HttpResult();
                sampleHttpResult.StartTime = TimeProvider.Current.UtcNow;
                sampleHttpResult.Request = "Mock REQUEST " + i;
                sampleHttpResult.Response = "Mock RESPONSE " + i;
                batchDetail.DetailHttpResult = sampleHttpResult;

                batch.Details.Add(batchDetail);
            }

            return batch;
        }


        public Batch GetMock(object uniqueIdentifier)
        {
            Batch batch = GetMock();
            batch.BatchId = uniqueIdentifier.ToString();
            return batch;
        }
    }
}