﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Domain.Test
{
    public class WorkerUnitTestInstaller : IWindsorInstaller
    {

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IWorkerContext>()
                    .ImplementedBy<UnitTestWorkerContext>()
            );

            container.Register(
                Classes
                    .FromThisAssembly()
                    .Where(t => t.Name.EndsWith("MockFactory"))
                    .WithServiceAllInterfaces()
            );

            container.Register(
               Classes
                   .FromAssemblyNamed("WorkerDataAccessTest")
                   .Where(t => t.Name.EndsWith("Repository"))
                   .WithServiceAllInterfaces()
            );

            container.Register(
                Classes
                    .FromAssemblyNamed("WorkerDataAccess")
                    .Where(t => t.Name.EndsWith("Repository"))
                    .WithServiceAllInterfaces()
            );

            container.Register(
                Classes
                    .FromAssemblyNamed("WorkerDomain")
                    .Where(t => t.Name.EndsWith("Service"))
                    .WithServiceAllInterfaces()
            );
        }
    }
}
