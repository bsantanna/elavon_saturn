﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Net.Mail;

namespace Saturn.Worker.Common
{
    public class MailService : AbstractMailService
    {
        private readonly IWorkerContext workerContext;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MailService(IWorkerContext workerContext)
        {
            log.Debug("Creating instance...");

            if(workerContext ==null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;
        }

        protected override MailAddress GetFrom()
        {
            return new MailAddress(workerContext.FromEmailAddress());
        }

        protected override string GetPassword()
        {
            return workerContext.EmailPassword();
        }

        protected override bool IsSSLEnabled()
        {
            return workerContext.EmailSSLEnabled();
        }

        protected override int GetPort()
        {
            return workerContext.EmailSMTPPort();
        }

        protected override string GetHost()
        {
            return workerContext.EmailHost();
        }
    }
}
