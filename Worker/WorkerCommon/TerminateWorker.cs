﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Common
{
    public class TerminateWorker : IWorker
    {
        private readonly IWorkerContext workerContext;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public TerminateWorker(IWorkerContext workerContext)
        {
            log.Debug("Creating instance...");

            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;
        }

        public void Start()
        {
            // do nothing
        }

        public void End(WorkSummary workSummary)
        {
            // do nothing
        }

        public void Work(WorkSummary workSummary)
        {
            // do nothing
        }

        public IWorkerContext GetWorkerContext()
        {
            return this.workerContext;
        }
    }
}
