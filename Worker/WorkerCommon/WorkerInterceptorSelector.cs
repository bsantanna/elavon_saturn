﻿using Castle.Core;
using Castle.MicroKernel.Proxy;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using Saturn.Worker.Error;
using Saturn.Worker.Logs;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Common
{
    public class WorkerInterceptorSelector : IModelInterceptorsSelector
    {

        public bool HasInterceptors(ComponentModel model)
        {
            return IsIWorkerInstance(model) || IsIBatchServiceInstance(model);
        }

        public InterceptorReference[] SelectInterceptors(ComponentModel model, InterceptorReference[] interceptors)
        {
            InterceptorReference[] selectedInterceptors = null;

            if (IsIWorkerInstance(model))
            {
                selectedInterceptors = new InterceptorReference[] {
                    InterceptorReference.ForType<WorkerErrorHandler>(),
                    InterceptorReference.ForType<WorkerExecutionLog>(),
                    InterceptorReference.ForType<MailNotificationInterceptor>(),
                    InterceptorReference.ForType<StorePendingWorkInterceptor>(),
                    InterceptorReference.ForType<CircuitBreakerInterceptor>()
                };
            }
            else if (IsIBatchServiceInstance(model))
            {
                selectedInterceptors = new InterceptorReference[]{
                    InterceptorReference.ForType<WorkerErrorHandler>(),
                    InterceptorReference.ForType<MailNotificationInterceptor>(),
                    InterceptorReference.ForType<BatchServiceAccessLog>(),
                    InterceptorReference.ForType<CircuitBreakerInterceptor>()
                };
            }
            else
            {
                selectedInterceptors = new InterceptorReference[] { };
            }

            return selectedInterceptors;
        }

        private bool IsIBatchServiceInstance(ComponentModel model)
        {
            return typeof(IBatchService).IsAssignableFrom(model.Implementation);
        }

        private bool IsIWorkerInstance(ComponentModel model)
        {
            return typeof(IWorker).IsAssignableFrom(model.Implementation);
        }
    }
}
