﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Common
{
    public abstract class AbstractConfigurationWorkerContext : IWorkerContext
    {
        public abstract string WorkerName();

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AbstractConfigurationWorkerContext()
        {
            log.Debug("Creating instance...");
        }

        public string SqlConnectionUri()
        {
            return ConfigurationManager.AppSettings["SqlConnectionUri"];
        }

        public int HttpKeepAlive()
        {
            int value = -1;
            var strValue = ConfigurationManager.AppSettings["HttpKeepAlive"];
            if (strValue != null)
            {
                value = Int32.Parse(strValue);
            }
            return value;
        }

        public string RegKey()
        {
            return ConfigurationManager.AppSettings["RegKey"];
        }

        public string ViaConnexUri()
        {
            return ConfigurationManager.AppSettings["ViaConnexUri"];
        }

        public int ViaConnexRetryTimeout()
        {
            int value = -1;
            var strValue = ConfigurationManager.AppSettings["ViaConnexRetryTimeout"];
            if (strValue != null)
            {
                value = Int32.Parse(strValue);
            }
            return value;
        }

        public string NotificationEmailAddress()
        {
            return ConfigurationManager.AppSettings["NotificationEmailAddress"];
        }

        public string FromEmailAddress()
        {
            return ConfigurationManager.AppSettings["FromEmailAddress"];
        }

        public string EmailPassword()
        {
            return ConfigurationManager.AppSettings["EmailPassword"];
        }

        public bool EmailSSLEnabled()
        {
            bool value = false;
            var strValue = ConfigurationManager.AppSettings["EmailSSLEnabled"];
            if (strValue != null)
            {
                value = Boolean.Parse(strValue);
            }
            return value;
        }

        public int EmailSMTPPort()
        {
            int value = -1;
            var strValue = ConfigurationManager.AppSettings["EmailSMTPPort"];
            if (strValue != null)
            {
                value = Int32.Parse(strValue);
            }
            return value;
        }

        public string EmailHost()
        {
            return ConfigurationManager.AppSettings["EmailHost"];
        }

        public string AppDataDirectory()
        {
            var strValue = ConfigurationManager.AppSettings["AppDataDirectory"];
            if (String.IsNullOrEmpty(strValue))
            {
                strValue = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            }
            return strValue;
        }

        public string FeedBaseUri()
        {
            return ConfigurationManager.AppSettings["FeedBaseUri"];
        }
    }
}
