﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Saturn.Worker.Common
{
    public class BusinessConstants
    {
        public enum ResponseCode
        {
            [Description("AA")]
            Approved,
            [Description("ND")]
            Declined,
            [Description("PP")]
            ProcessPending,
            [Description("E0")]
            ErrorXMLStructure,
            [Description("E1")]
            InvalidMerchantCredentials,
            [Description("E2")]
            MerchantDisabled,
            [Description("E3")]
            TransactionNotFound,
            [Description("E4")]
            TransactionDuplicated,
            [Description("E5")]
            TransactionTypeNotAllowed,
            [Description("E6")]
            IncorrectData,
            [Description("E7")]
            TransactionStatusNotAllowedOperation,
            [Description("E8")]
            MissingRequiredData,
            [Description("EX")]
            ExpiredRequest,
            [Description("UE")]
            UnexpectedError,
            [Description("SU")]
            SystemUnavailable,
            [Description("TO")]
            TimeOut
        }

        public static string GetDescriptionValue(Enum constant)
        {
            FieldInfo fi = constant.GetType().GetField(constant.ToString());
            DescriptionAttribute attribute = fi.GetCustomAttributes(typeof(DescriptionAttribute), false).Single() as DescriptionAttribute;
            if (attribute != null)
            {
                return attribute.Description;
            }
            else
            {
                return constant.ToString();
            }
        }
    }
}
