﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Common
{
    public class WorkerCommonInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes
                    .FromAssemblyNamed("WorkerDomain")
                    .Where(t => t.Name.EndsWith("Service"))
                    .WithServiceAllInterfaces()
            );

            container.Register(
                Classes
                    .FromAssemblyNamed("WorkerDataAccess")
                    .Where(t => t.Name.EndsWith("Repository"))
                    .WithServiceAllInterfaces()
            );

            container.Register(
                Component
                    .For<IClaimChecker<Batch>>()
                    .ImplementedBy<BatchClaimChecker>()
            );

            container.Register(
                Component
                    .For<IMailService>()
                    .ImplementedBy<MailService>()
            );

            container.Register(
                Component
                    .For<IWorker>()
                    .ImplementedBy<TerminateWorker>()
            );

            container.Kernel.ProxyFactory.AddInterceptorSelector(new WorkerInterceptorSelector());
        }
    }
}
