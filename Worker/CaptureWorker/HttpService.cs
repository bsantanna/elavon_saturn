﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Capture
{
    public class HttpService : AbstractHttpService
    {
        private readonly IWorkerContext workerContext;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public HttpService(IWorkerContext workerContext)
        {
            log.Debug("Creating instance...");

            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;
        }

        public override Dictionary<string, string> GetHeaders()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Connection", "keep-alive");
            headers.Add("Keep-Alive", workerContext.HttpKeepAlive().ToString());
            headers.Add("Registration-Key", workerContext.RegKey());
            return headers;
        }

        protected override string MaskRequestData(string rawRequest)
        {
            string ViaConexValuePair = rawRequest;

            // hxamara code from Utils.UT in legacy code
            int p1;
            //Numero do cartao
            p1 = ViaConexValuePair.IndexOf("Account_Data=");
            if (p1 >= 0)
            {
                p1 = p1 + 13;
                int p2 = ViaConexValuePair.IndexOf('&', p1);
                if (p2 < 0)
                {
                    p2 = ViaConexValuePair.IndexOf(Environment.NewLine, p1);
                }
                string[] card = null;
                string NewCard = "";
                string NewCardData = "";
                string CardInfo = ViaConexValuePair.Substring(p1, p2 - p1);
                if (CardInfo.IndexOf('^') >= 0) //trilha 1
                {
                    card = CardInfo.Split('^');
                    NewCard = card[0].Substring(0, 1) + NewCard.PadLeft(card[0].Length - 4, '*') + card[0].Substring(card[0].Length - 4, 4);
                    NewCardData = NewCard + "^";
                    NewCardData += card[1] + "^";
                    NewCardData += "... (Track1)";
                    ViaConexValuePair = ViaConexValuePair.Replace(CardInfo, NewCardData);

                }
                else if (CardInfo.IndexOf('=') >= 0) //trilha 2 ou digitado
                {
                    card = CardInfo.Split('=');
                    NewCard = NewCard.PadLeft(card[0].Length - 4, '*') + card[0].Substring(card[0].Length - 4, 4);
                    NewCardData = NewCard + "=";
                    if (card.Length == 2 & card[1].Length == 4) //digitado
                    {
                        NewCardData += card[1];
                    }
                    else
                    {
                        NewCardData += "... (Track2)";
                    }
                    ViaConexValuePair = ViaConexValuePair.Replace(CardInfo, NewCardData);

                }
                else //só cartão
                {
                    NewCard = NewCard.PadLeft(CardInfo.Length - 4, '*') + CardInfo.Substring(CardInfo.Length - 4);
                    ViaConexValuePair = ViaConexValuePair.Replace(CardInfo, NewCard);
                }
            }
            //Numero do cartao

            string labelToMatch = "CVV2_Value=";
            p1 = ViaConexValuePair.IndexOf(labelToMatch);
            if (p1 >= 0)
            {
                p1 = p1 + labelToMatch.Length;
                int p2 = ViaConexValuePair.IndexOf('&', p1);
                if (p2 < 0)
                {
                    p2 = ViaConexValuePair.IndexOf(Environment.NewLine, p1);
                }
                string CVV2Info = ViaConexValuePair.Substring(p1, p2 - p1);
                ViaConexValuePair = ViaConexValuePair.Replace(labelToMatch + CVV2Info, labelToMatch + new string('*', CVV2Info.Length));
            }

            labelToMatch = "PIN_Block=";
            p1 = ViaConexValuePair.IndexOf(labelToMatch);
            if (p1 >= 0)
            {
                p1 = p1 + labelToMatch.Length;
                int p2 = ViaConexValuePair.IndexOf('&', p1);
                if (p2 < 0)
                {
                    p2 = ViaConexValuePair.IndexOf(Environment.NewLine, p1);
                }
                string PINBlock = ViaConexValuePair.Substring(p1, p2 - p1);
                ViaConexValuePair = ViaConexValuePair.Replace(labelToMatch + PINBlock, labelToMatch + new string('*', 16));
            }

            labelToMatch = "KSN=";
            p1 = ViaConexValuePair.IndexOf(labelToMatch);
            if (p1 >= 0)
            {
                p1 = p1 + labelToMatch.Length;
                int p2 = ViaConexValuePair.IndexOf('&', p1);
                if (p2 < 0)
                {
                    p2 = ViaConexValuePair.IndexOf(Environment.NewLine, p1);
                }
                string KSN = ViaConexValuePair.Substring(p1, p2 - p1);
                ViaConexValuePair = ViaConexValuePair.Replace(labelToMatch + KSN, labelToMatch + new string('*', 16));
            }

            return ViaConexValuePair;
        }
    }
}
