﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Saturn.Core.API;
using Saturn.Worker.Capture;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed;
using System;
using System.Collections.Generic;
using System.ServiceProcess;


namespace Saturn.Worker.Capture
{
    public class CaptureWorkerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IWorkerContext>()
                    .ImplementedBy<CaptureWorkerContext>()
            );

            container.Register(
                Component
                    .For<IBatchTransformer<string>>()
                    .ImplementedBy<HeaderTransformer>()
            );

            container.Register(
                Component
                    .For<IBatchTransformer<string>>()
                    .ImplementedBy<TrailerTransformer>()
            );

            container.Register(
                Component
                    .For<IBatchDetailTransformer<string>>()
                    .ImplementedBy<DetailTransformer>()
            );

            container.Register(
                Component
                    .For<IHttpResultValidator>()
                    .ImplementedBy<BatchSegmentHttpResultValidator>()
            );

            container.Register(
                Component
                    .For<IHttpResultValidator>()
                    .ImplementedBy<TrailerHttpResultValidator>()
            );

            container.Register(
                Component
                    .For<ICaptureService>()
                    .ImplementedBy<CaptureService>()
                    .DependsOn(new Dependency[]{
                        Dependency.OnComponent("trailerHttpResultValidator", typeof(TrailerHttpResultValidator)),
                        Dependency.OnComponent("trailerTransformer", typeof(TrailerTransformer))
                    })
            );

            container.Register(
                Component
                    .For<ServiceBase>()
                    .ImplementedBy<CaptureWorker>()
                    .DependsOn(new Dependency[] { 
                        Dependency.OnComponent("feedWriter", typeof(FeedWriter))
                    })
            );
        }
    }
}
