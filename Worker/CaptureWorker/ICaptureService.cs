﻿using Saturn.Core.API;
using Saturn.Worker.Domain;

namespace Saturn.Worker.Capture
{
    public interface ICaptureService
    {
        string GetHeader(Batch batch);

        string GetDetail(Batch batch, BatchDetail batchDetail);

        string GetTrailer(Batch batch);
        
        bool IsBatchValid(Batch batch);

        bool IsDetailValid(HttpResult result);
        
        bool IsHeaderValid(HttpResult result);

        bool IsTrailerValid(HttpResult result);

    }
}
