﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Saturn.Core.API;

namespace Saturn.Worker.Capture
{
    public class CaptureWorkerRuntimeInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IHttpService>()
                    .ImplementedBy<HttpService>()
            );
        }
    }
}
