﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Saturn.Worker.Capture
{
    public class CaptureWorker : AbstractWorker
    {
        private readonly ICaptureService captureService;
        private readonly IHttpService httpService;
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CaptureWorker(IWorkerContext workerContext, IBatchService batchService, ICaptureService captureService, IHttpService httpService, IMailService mailService, IWorker feedWriter)
            : base(workerContext, mailService, batchService, feedWriter)
        {
            log.Debug("Creating instance...");

            if (captureService == null)
            {
                throw new ArgumentNullException("captureService");
            }
            this.captureService = captureService;

            if (httpService == null)
            {
                throw new ArgumentNullException("httpService");
            }
            this.httpService = httpService;
        }

        private IList<BatchDetail> ProcessDetails(Uri uri, Stream stream, Batch batch, IEnumerable<BatchDetail> details)
        {
            // creates a temporary list with already processed details
            IList<BatchDetail> processedDetails = new List<BatchDetail>();

            // iterate over details
            foreach (BatchDetail detail in details)
            {
                // send detail data
                string detailData = this.captureService.GetDetail(batch, detail);
                detail.DetailHttpResult = this.httpService.Post(uri, stream, detailData);

                // validate response;
                if (this.captureService.IsDetailValid(detail.DetailHttpResult))
                {
                    // insert details in processed
                    processedDetails.Add(detail);
                }
                else
                {
                    // 1- Store invalid detail
                    batch.InvalidDetails.Add(detail);

                    // 2- break loop
                    break;
                }
            }

            return processedDetails;
        }

        public IList<BatchDetail> RebuildDetails(IEnumerable<BatchDetail> processedDetails, IList<BatchDetail> allDetails)
        {
            // create a new list
            IList<BatchDetail> rebuiltDetails = new List<BatchDetail>();

            // append processed details
            foreach (var detail in processedDetails)
            {
                rebuiltDetails.Add(detail);
            }

            // append non processed details
            for (var i = processedDetails.Count() + 1; i < allDetails.Count(); i++)
            {
                rebuiltDetails.Add(allDetails[i]);
            }

            return rebuiltDetails;
        }

        private void HandleInvalidBatch(WorkSummary workSummary, Batch batch, String reason)
        {
            // store invalid batch
            workSummary.InvalidBatches.Add(batch);

            // log
            string logMessage = "Error while processing Batch[" + batch.BatchId + "], reason: " + reason;
            this.eventLog.WriteEntry(logMessage, EventLogEntryType.Error);
            log.Warn(logMessage);

            // sent mail notification
            string subject = "[ERROR] Invalid Batch: " + batch.BatchId;
            this.mailService.SendMail(workerContext.NotificationEmailAddress(), subject, logMessage);
        }

        public override void Work(WorkSummary workSummary)
        {
            string logMessage = "Starting to process";
            this.eventLog.WriteEntry(logMessage);
            log.Debug(logMessage);

            // get uri from context
            Uri uri = new Uri(this.workerContext.ViaConnexUri());

            // verifies if this workSummary has pending batches (error recovery case)
            if (workSummary.PendingBatches.Count <= 0)
            {
                // enqueue batches from batchService
                foreach (var batch in this.batchService.GetBatches())
                {
                    workSummary.PendingBatches.Enqueue(batch);
                }

                logMessage = "Recovered " + workSummary.PendingBatches.Count + " batches from datasource.";
                this.eventLog.WriteEntry(logMessage);
                log.Debug(logMessage);
            }
            else
            {
                logMessage = "Processing " + workSummary.PendingBatches.Count + " pending batches from last execution.";
                this.eventLog.WriteEntry(logMessage, EventLogEntryType.Warning);
                log.Warn(logMessage);
            }

            // store partial work summary
            this.batchService.StoreWorkSummary(workSummary);

            // iterates in each
            while (workSummary.PendingBatches.Count > 0)
            {
                var batch = workSummary.PendingBatches.Peek();

                // claim check process
                if (this.captureService.IsBatchValid(batch))
                {
                    // create a managed (IDisposable) tcpClient (finally clause closes connection)
                    using (var tcpClient = this.httpService.GetTcpClient(uri))
                    {
                        // open connection stream
                        Stream stream = this.httpService.OpenStream(uri, tcpClient);

                        // send batch header
                        string headerData = this.captureService.GetHeader(batch);
                        batch.HeaderHttpResult = this.httpService.Post(uri, stream, headerData);

                        // validate response
                        if (this.captureService.IsHeaderValid(batch.HeaderHttpResult))
                        {
                            // transient store for batch recovery (when a detail has an error)
                            IList<BatchDetail> processedDetails = ProcessDetails(uri, stream, batch, batch.Details);

                            // verify if all details where processed
                            if (processedDetails.Count() != batch.Details.Count())
                            {
                                // create a new batch
                                Batch recoveredBatch = new Batch();
                                recoveredBatch.Version = batch.Version;
                                recoveredBatch.ApplicationId = batch.ApplicationId;
                                recoveredBatch.TerminalId = batch.TerminalId;
                                recoveredBatch.BatchNumber = batch.BatchNumber;
                                recoveredBatch.Details = RebuildDetails(processedDetails, batch.Details);
                                recoveredBatch.InvalidDetails = batch.InvalidDetails;

                                // inserts new batch into queue
                                workSummary.PendingBatches.Enqueue(recoveredBatch);

                                // log recovery
                                BatchDetail detainInError = batch.Details[processedDetails.Count()];
                                logMessage = "Error while processing Transaction[" + detainInError.TransactionID + "] from Batch[" + batch.BatchId + "], rebuilding batch without this transaction.";
                                this.eventLog.WriteEntry(logMessage, EventLogEntryType.Warning);
                                log.Warn(logMessage);
                                log.Debug("Server HTTP Response: " + detainInError.DetailHttpResult.Response);
                            }

                            else
                            {
                                // updates details on batch
                                batch.Details = processedDetails;

                                // all details where processed
                                // send batch trailer
                                string trailerData = this.captureService.GetTrailer(batch);
                                batch.TrailerHttpResult = this.httpService.Post(uri, stream, trailerData);

                                // validate response
                                if (this.captureService.IsTrailerValid(batch.TrailerHttpResult))
                                {
                                    logMessage = "Processed Batch[" + batch.BatchId + "] successfully.";
                                    this.eventLog.WriteEntry(logMessage);
                                    log.Debug(logMessage);

                                    // store processed batch
                                    workSummary.ProcessedBatches.Add(batch);
                                }
                                else
                                {
                                    this.HandleInvalidBatch(workSummary, batch, " Invalid Batch Trailer Response: " + batch.TrailerHttpResult.Response);
                                }
                            }
                        }
                        else
                        {
                            this.HandleInvalidBatch(workSummary, batch, "Invalid Batch Header Response: " + batch.HeaderHttpResult.Response);
                        }
                    }
                }
                else
                {
                    this.HandleInvalidBatch(workSummary, batch, "Failed claim check, probably this batch has already been processed");
                }

                // remove batch from queue
                workSummary.PendingBatches.Dequeue();

                // check for new batches
                foreach (var newBatch in this.batchService.GetBatches())
                {
                    workSummary.PendingBatches.Enqueue(newBatch);
                }

                // store partial work summary
                this.batchService.StoreWorkSummary(workSummary);
            }
        }
    }
}
