﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Text;
using System.Web;

namespace Saturn.Worker.Capture
{
    public class HeaderTransformer : IBatchTransformer<string>
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public HeaderTransformer()
        {
            log.Debug("Creating instance...");
        }

        public string Transform(Batch batch)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Request=" + HttpUtility.UrlEncode("Batch.Detail Header") + "&");
            stringBuilder.Append("Version=" + HttpUtility.UrlEncode(batch.Version) + "&");
            stringBuilder.Append("HD.Network_Status_Byte=" + HttpUtility.UrlEncode("#") + "&");
            stringBuilder.Append("HD.Application_ID=" + HttpUtility.UrlEncode(batch.ApplicationId) + "&");
            stringBuilder.Append("HD.Terminal_ID=" + HttpUtility.UrlEncode(batch.TerminalId) + "&");
            stringBuilder.Append("90.Batch_Number=" + HttpUtility.UrlEncode(batch.BatchNumber) + "&");
            stringBuilder.Append("90.Batch_Number=000&");
            stringBuilder.Append("90.Record_Count=" + (batch.Details.Count + 2) + "&"); // + 2 means 1 for header and 1 for trailer
            stringBuilder.Append("90.Net_Amount=" + batch.GetTotalAmount() + "&");
            stringBuilder.Append("90.Net_Tip_Amount=0");
            return stringBuilder.ToString();
        }
    }
}
