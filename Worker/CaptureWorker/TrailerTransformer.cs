﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Text;
using System.Web;

namespace Saturn.Worker.Capture
{
    public class TrailerTransformer: IBatchTransformer<string>
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public TrailerTransformer()
        {
            log.Debug("Creating instance...");
        }

        public string Transform(Batch batch)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Request=" + HttpUtility.UrlEncode("Batch.Detail Trailer") + "&");
            stringBuilder.Append("Version=" + HttpUtility.UrlEncode(batch.Version) + "&");
            stringBuilder.Append("HD.Network_Status_Byte=" + HttpUtility.UrlEncode("*") + "&");
            stringBuilder.Append("HD.Application_ID=" + HttpUtility.UrlEncode(batch.ApplicationId) + "&");
            stringBuilder.Append("HD.Terminal_ID=" + HttpUtility.UrlEncode(batch.TerminalId) + "&");
            stringBuilder.Append("98.Transmission_Date=" + TimeProvider.Current.UtcNow.ToString("MMdd") + "&");
            stringBuilder.Append("98.Record_Count=" + (batch.Details.Count + 2) + "&");
            stringBuilder.Append("98.Net_Deposit=" + batch.GetTotalAmount() + "&");
            stringBuilder.Append("98.Hash_Total=" + batch.GetTotalAmount());
            return stringBuilder.ToString();
        }
    }
}
