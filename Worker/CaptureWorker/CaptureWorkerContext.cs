﻿using log4net;
using Saturn.Worker.Common;

namespace Saturn.Worker.Capture
{
    public class CaptureWorkerContext : AbstractConfigurationWorkerContext
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CaptureWorkerContext()
            : base()
        {
            log.Debug("Creating instance...");
        }

        public override string WorkerName()
        {
            return "Capture";
        }
    }
}
