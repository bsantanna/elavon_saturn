﻿using log4net;
using Saturn.Core.API;

namespace Saturn.Worker.Capture
{
    public class TrailerHttpResultValidator : IHttpResultValidator
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public TrailerHttpResultValidator()
        {
            log.Debug("Creating instance...");
        }

        public bool IsValid(HttpResult result)
        {
            return result.Response.Contains("89.Response_Message=GB") || result.Response.Contains("89.Response_Message=FECHA");
        }
    }
}
