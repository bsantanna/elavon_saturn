﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;

namespace Saturn.Worker.Capture
{
    public class CaptureService : ICaptureService
    {
        private readonly IBatchTransformer<string> headerTransformer;
        private readonly IBatchDetailTransformer<string> detailTransformer;
        private readonly IBatchTransformer<string> trailerTransformer;
        private readonly IClaimChecker<Batch> batchClaimChecker;
        private readonly IHttpResultValidator batchSegmentHttpResultValidator;
        private readonly IHttpResultValidator trailerHttpResultValidator;
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CaptureService(IBatchTransformer<string> headerTransformer, IBatchDetailTransformer<string> detailTransformer, IBatchTransformer<string> trailerTransformer, IClaimChecker<Batch> batchClaimChecker, IHttpResultValidator batchSegmentHttpResultValidator, IHttpResultValidator trailerHttpResultValidator)
        {
            log.Debug("Creating instance...");

            if (headerTransformer == null)
            {
                throw new ArgumentNullException("headerTransformer");
            }
            this.headerTransformer = headerTransformer;

            if (detailTransformer == null)
            {
                throw new ArgumentNullException("detailTransformer");
            }
            this.detailTransformer = detailTransformer;

            if (trailerTransformer == null)
            {
                throw new ArgumentNullException("trailerTransformer");
            }
            this.trailerTransformer = trailerTransformer;

            if (batchClaimChecker == null)
            {
                throw new ArgumentNullException("batchClaimChecker");
            }
            this.batchClaimChecker = batchClaimChecker;

            if (batchSegmentHttpResultValidator == null)
            {
                throw new ArgumentNullException("batchSegmentHttpResultValidator");
            }
            this.batchSegmentHttpResultValidator = batchSegmentHttpResultValidator;

            if (trailerHttpResultValidator == null)
            {
                throw new ArgumentNullException("trailerHttpResultValidator");
            }
            this.trailerHttpResultValidator = trailerHttpResultValidator;
        }

        public string GetHeader(Batch batch)
        {
            return this.headerTransformer.Transform(batch);
        }

        public string GetTrailer(Batch batch)
        {
            return this.trailerTransformer.Transform(batch);
        }

        public string GetDetail(Batch batch, BatchDetail batchDetail)
        {
            return this.detailTransformer.Transform(batch, batchDetail);
        }

        public bool IsBatchValid(Batch batch)
        {
            return this.batchClaimChecker.IsValid(batch);
        }

        public bool IsHeaderValid(HttpResult result)
        {
            return batchSegmentHttpResultValidator.IsValid(result);
        }

        public bool IsDetailValid(HttpResult result)
        {
            return batchSegmentHttpResultValidator.IsValid(result);
        }

        public bool IsTrailerValid(HttpResult result)
        {
            return trailerHttpResultValidator.IsValid(result);
        }
    }
}
