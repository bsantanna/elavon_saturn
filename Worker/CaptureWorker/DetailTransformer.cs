﻿using Saturn.Worker.Domain.BusinessEntity;
using Saturn.Worker.Domain;
using System;
using System.Text;
using System.Web;
using System.Collections.Generic;
using Saturn.Core.API;
using System.Text.RegularExpressions;
using System.Xml;
using log4net;

namespace Saturn.Worker.Capture
{
    public class DetailTransformer : IBatchDetailTransformer<string>
    {
        private readonly IXmlService xmlService;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DetailTransformer(IXmlService xmlService)
        {
            log.Debug("Creating instance...");

            if (xmlService == null)
            {
                throw new ArgumentNullException("xmlService");
            }
            this.xmlService = xmlService;
        }

        public string Transform(Batch batch, BatchDetail batchDetail)
        {
            Dictionary<string, string> detailDictionary = new Dictionary<string, string>();

            detailDictionary.Add("Request", "Batch.Detail");
            detailDictionary.Add("Version", batch.Version);
            detailDictionary.Add("HD.Network_Status_Byte", "#");
            detailDictionary.Add("HD.Application_ID", batch.ApplicationId);
            detailDictionary.Add("HD.Terminal_ID", batch.TerminalId);
            detailDictionary.Add("04.Transaction_Code", batchDetail.Request);

            if (!String.IsNullOrEmpty(batchDetail.NSUHost))
            {
                detailDictionary.Add("03.Merchant_Reference_Nbr", batchDetail.NSUHost);
            }

            // do not send E-Commerce_Indicator for Dinners and Discover
            if (!String.IsNullOrEmpty(batchDetail.CardProduct))
            {
                if (!batchDetail.CardProduct.Equals(CardProductType.DinersCredit) &&
                    !batchDetail.CardProduct.Equals(CardProductType.DiscoverCredit))
                {
                    // translate from WSGate indicator code to ViaConnex indicator code
                    string eci = null;
                    if (batchDetail.CardProduct.Equals("2") || batchDetail.CardProduct.Equals("5"))
                    {
                        eci = "1";
                    }
                    else if (batchDetail.CardProduct.Equals("0") || batchDetail.CardProduct.Equals("7"))
                    {
                        eci = "3";
                    }
                    else if (batchDetail.CardProduct.Equals("1") || batchDetail.CardProduct.Equals("6"))
                    {
                        eci = "2";
                    }

                    if (!String.IsNullOrEmpty(eci))
                    {
                        detailDictionary.Add("13.E-Commerce_Indicator", eci);
                    }

                }

                if (!batchDetail.CardProduct.Contains("Debit"))
                {
                    detailDictionary.Add("93.AVS_Response", " ");
                    if (!String.IsNullOrEmpty(batchDetail.CVV2Response))
                    {
                        detailDictionary.Add("93.CVV2_Response", batchDetail.CVV2Response);
                    }
                }
                else
                {
                    detailDictionary.Add("94.Debit-EBT_Network_ID", String.IsNullOrEmpty(batchDetail.DebitEBTNetworkID) ? " " : batchDetail.DebitEBTNetworkID);
                    detailDictionary.Add("94.Debit-EBT_Settlement_Date", String.IsNullOrEmpty(batchDetail.DebitEBTSettlementDate) ? "0000" : batchDetail.DebitEBTSettlementDate);
                    detailDictionary.Add("94.Debit_Interchange_Indicator", String.IsNullOrEmpty(batchDetail.DebitInterchangeIndicator) ? " " : batchDetail.DebitInterchangeIndicator);
                }

            }

            if (!String.IsNullOrEmpty(batchDetail.ThreeDSAuthenticationValue))
            {
                detailDictionary.Add("13.DDD_Secure_Value", batchDetail.ThreeDSAuthenticationValue);
            }

            if (!String.IsNullOrEmpty(batchDetail.ECI))
            {
                string ucaf = null;

                if (batchDetail.ECI.Equals("0") || batchDetail.ECI.Equals("1") || batchDetail.ECI.Equals("2"))
                {
                    ucaf = batchDetail.ECI;
                }

                if (!String.IsNullOrEmpty(ucaf))
                {
                    detailDictionary.Add("13.UCAF_Indicator", ucaf);
                }
            }

            if (!String.IsNullOrEmpty(batchDetail.Recurring) && batchDetail.Recurring.Equals("1"))
            {
                detailDictionary.Add("14.Recurring_Payment_Type", batchDetail.Recurring);
                detailDictionary.Add("92.Capture_Tran_Code", "2");
            }
            else
            {
                string recurringType = null;
                if (!String.IsNullOrEmpty(batchDetail.InstallmentsRecurring))
                {
                    if (batchDetail.InstallmentsRecurring.Equals("M"))
                    {
                        recurringType = "3";
                    }
                    else if (batchDetail.InstallmentsRecurring.Equals("I"))
                    {
                        recurringType = "4";
                    }
                }

                if (!String.IsNullOrEmpty(recurringType))
                {
                    detailDictionary.Add("14.Recurring_Payment_Type", recurringType);
                }


                string captureTranCode;
                if (batchDetail.Request.Equals("Debit.Purchase"))
                {
                    captureTranCode = "3";
                }
                else
                {
                    captureTranCode = "5";
                }
                detailDictionary.Add("92.Capture_Tran_Code", captureTranCode);

                if (!String.IsNullOrEmpty(batchDetail.Installments))
                {
                    detailDictionary.Add("14.Installment_Count", batchDetail.Installments);
                }

            }

            if (!String.IsNullOrEmpty(batchDetail.CaptureDetailsInstallments))
            {
                detailDictionary.Add("14.Recurring_Payment_Type", "3");
                detailDictionary.Add("14.Installment_Count", batchDetail.CaptureDetailsInstallments);
            }

            if (!String.IsNullOrEmpty(batchDetail.PurchaseDepartureDate))
            {
                detailDictionary.Add("21.Departure_Date", batchDetail.PurchaseDepartureDate);
                detailDictionary.Add("21.Completion_Date", batchDetail.PurchaseDepartureDate);
            }

            detailDictionary.Add("92.Record_Number", (batch.Details.IndexOf(batchDetail) + 1).ToString());

            if (!String.IsNullOrEmpty(batchDetail.CardData))
            {
                string[] card;
                string cardNumber = null;
                string cardExp = null;

                if (batchDetail.CardData.IndexOf("^") >= 0)
                {
                    card = batchDetail.CardData.Split('^');
                    cardNumber = card[0].Substring(1, card[0].Length - 1);
                    cardExp = card[2].Substring(2, 2) + card[2].Substring(0, 2);
                }
                else
                {
                    card = batchDetail.CardData.Split('=');
                    cardNumber = card[0];

                    Regex regex = new Regex(@"^[0-9]+$");

                    if (regex.IsMatch(card[1]) && card[1].Length == 4)
                    {
                        cardExp = card[1];
                    }
                    else
                    {
                        cardExp = card[1].Substring(2, 2) + card[1].Substring(0, 2);
                    }
                }

                string accountData = String.IsNullOrEmpty(cardExp) ? cardNumber : cardNumber + "=" + cardExp;
                detailDictionary.Add("92.Account_Data", accountData);
            }

            if (!String.IsNullOrEmpty(batchDetail.CaptureDetailsAmount))
            {
                detailDictionary.Add("92.Transaction_Amount", batchDetail.CaptureDetailsAmount);
            }

            if (!String.IsNullOrEmpty(batchDetail.AuthAmount))
            {
                detailDictionary.Add("92.Original_Auth_Amount", batchDetail.AuthAmount);
            }

            if (!String.IsNullOrEmpty(batchDetail.ApprovalCode))
            {
                detailDictionary.Add("92.Approval_Code", batchDetail.ApprovalCode);
            }

            if (!String.IsNullOrEmpty(batchDetail.AuthorizationDateTime))
            {
                string date = DateTime.Parse(batchDetail.AuthorizationDateTime).ToString("MMddyy");
                string time = DateTime.Parse(batchDetail.AuthorizationDateTime).ToString("HHmmss");
                detailDictionary.Add("92.Authorization_Date", date);
                detailDictionary.Add("92.Authorization_Time", time);
            }

            if (!String.IsNullOrEmpty(batchDetail.POSEntryCapability))
            {
                detailDictionary.Add("92.POS_Entry_Capability", batchDetail.POSEntryCapability);
            }

            if (!String.IsNullOrEmpty(batchDetail.CardEntryMode))
            {
                detailDictionary.Add("92.Account_Entry_Mode", batchDetail.CardEntryMode);

                string accountSource = null;
                if (batchDetail.CardEntryMode.Equals("01") || batchDetail.CardEntryMode.Equals("02"))
                {
                    accountSource = "0";
                }
                else if (batchDetail.CardEntryMode.Equals("03") || batchDetail.CardEntryMode.Equals("04"))
                {
                    accountSource = "3";
                }
                else
                {
                    accountSource = "1";
                }

                if (!String.IsNullOrEmpty(accountSource))
                {
                    detailDictionary.Add("92.Account_Source", accountSource);
                }

                detailDictionary.Add("92.Authorization_Source", String.IsNullOrEmpty(batchDetail.AuthorizationSource) ? " " : batchDetail.AuthorizationSource);

            }

            if (!String.IsNullOrEmpty(batchDetail.PINBlockDetails))
            {
                detailDictionary.Add("92.Card_ID", "A");
            }
            else if (!String.IsNullOrEmpty(batchDetail.CardEntryMode) && batchDetail.CardEntryMode.Equals("01"))
            {
                detailDictionary.Add("92.Card_ID", "N");
            }
            else
            {
                detailDictionary.Add("92.Card_ID", "@");
            }

            if (!String.IsNullOrEmpty(batchDetail.PS2000Data))
            {
                detailDictionary.Add("93.PS2000_Data", batchDetail.PS2000Data);
            }

            if (!String.IsNullOrEmpty(batchDetail.MSDI))
            {
                detailDictionary.Add("93.MSDI", batchDetail.MSDI);
            }

            if (!String.IsNullOrEmpty(batchDetail.ICCDetails))
            {
                // extract nodes from xml fragment
                XmlDocument iccDetailsXmlFragment = this.xmlService.GetXmlDocument(batchDetail.ICCDetails);
                detailDictionary.Add("97.ICC_Cryptogram", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/Cryptogram"));
                detailDictionary.Add("97.ICC_CID", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/CID"));
                detailDictionary.Add("97.ICC_Unpredictable_Number", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/UN"));
                detailDictionary.Add("97.ICC_ATC", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/ATC"));
                detailDictionary.Add("97.ICC_TVR", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/TVR"));
                detailDictionary.Add("97.ICC_TT", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/TT"));
                detailDictionary.Add("97.ICC_AIP", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/AIP"));
                detailDictionary.Add("97.ICC_TCP", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/TCP"));
                detailDictionary.Add("97.ICC_CVMR", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/CVMR"));
                detailDictionary.Add("97.ICC_TTD", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/TTD"));
                detailDictionary.Add("97.ICC_Terminal_Type", this.xmlService.GetValue(iccDetailsXmlFragment, ""));
                detailDictionary.Add("97.ICC_CSN", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/TerminalType"));
                detailDictionary.Add("97.ICC_AED", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/CSN"));
                detailDictionary.Add("97.ICC_IAD", this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/AED"));

                detailDictionary.Add("97.ICC_Authorized_Amount", batchDetail.AuthAmount);

                string otherAmount = this.xmlService.GetValue(iccDetailsXmlFragment, "/EMV/OtherAmount");
                detailDictionary.Add("97.ICC_Other_Amount", String.IsNullOrEmpty(otherAmount) ? "000000000000" : otherAmount);

                if (!String.IsNullOrEmpty(batchDetail.AuthAmountCurrencyCode))
                {
                    string numericCurrencyCode = null;

                    if (batchDetail.AuthAmountCurrencyCode.Equals("USD"))
                    {
                        numericCurrencyCode = "840";
                    }
                    else
                    {
                        numericCurrencyCode = "986";
                    }

                    if (!String.IsNullOrEmpty(numericCurrencyCode))
                    {
                        detailDictionary.Add("97.ICC_Transaction_Currency", numericCurrencyCode);
                    }
                }
            }

            if (!String.IsNullOrEmpty(batchDetail.MerchantDynamicDBA))
            {
                detailDictionary.Add("03.Dynamic_DBA_Name",batchDetail.MerchantDynamicDBA);
            }

            if (!String.IsNullOrEmpty(batchDetail.PurchaseAdditionalID))
            {
                detailDictionary.Add("B1.ROC_Text_Data", batchDetail.PurchaseAdditionalID);
            }

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var key in detailDictionary.Keys)
            {
                stringBuilder.Append(key + "=" + HttpUtility.UrlEncode(detailDictionary[key]) + "&");
            }

            return stringBuilder.ToString();
        }

    }
}
