﻿using Saturn.Core.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Feed
{
    public interface IFeedRepository<T>
    {
        string ArchiveFeed(SyndicationFeed workingFeed, Uri baseAddress, UriTemplate feedTemplate, DateTime archiveDate);
        SyndicationFeed GetWorkingFeed(Uri baseAddress, UriTemplate feedTemplate, string parentId);
        string GetWorkingFeedPath(string parentId);
        void StoreWorkingFeed(SyndicationFeed workingFeed, string workingFeedPath);
    }
}
