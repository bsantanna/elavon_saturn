﻿using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel.Syndication;

namespace Saturn.Worker.Feed
{
    public class BatchDetailFeed : AbstractWorkerFeed<BatchDetail>
    {
        private readonly string workSummaryFeedId;

        private readonly string batchFeedId;

        private readonly WorkSummary workSummary;

        private readonly Batch batch;

        public BatchDetailFeed(IWorkerContext workerContext, IFeedRepository<BatchDetail> feedRepository, string workSummaryFeedId, WorkSummary workSummary, string batchFeedId, Batch batch)
            : base(workerContext, feedRepository, new UriTemplate("/{y}/{m}/{d}/" + workSummaryFeedId + "/" + batchFeedId + "/feed.xml"), new UriTemplate("/{y}/{m}/{d}/" + workSummaryFeedId + "/" + batchFeedId + "/{id}/feed.xml"), workSummaryFeedId + "/" + batchFeedId)
        {
            if (workSummaryFeedId == null)
            {
                throw new ArgumentNullException("workSummaryFeedId");
            }
            this.workSummaryFeedId = workSummaryFeedId;

            if (workSummary == null)
            {
                throw new ArgumentNullException("workSummary");
            }
            this.workSummary = workSummary;

            if (batchFeedId == null)
            {
                throw new ArgumentNullException("batchFeedId");
            }
            this.batchFeedId = batchFeedId;

            if (batch == null)
            {
                throw new ArgumentNullException("batch");
            }
            this.batch = batch;

            // validation
            if (!workSummary.ProcessedBatches.Contains(batch)  && !workSummary.PendingBatches.Contains(batch) && !workSummary.InvalidBatches.Contains(batch))
            {
                throw new ArgumentException("Injected Batch instance does not belong to injected WorkSummary instance.");
            }

            // add element extension
            this.AddElementExtension<Batch>(batch);
        }

        public override string AddEntry(BatchDetail batchDetail)
        {
            string entryId = GetId(batchDetail);

            SyndicationItem item = new SyndicationItem
            {
                Id = entryId,
                Title = SyndicationContent.CreatePlaintextContent("TransactionID => " + batchDetail.TransactionID),
                LastUpdatedTime = workSummary.EndTime
            };

            // category
            item.Categories.Add(CreateCategory(typeof(IWorker).Name, workSummary.WorkerName));

            bool belongsToBatch = false;
            if (batch.Details.Contains(batchDetail))
            {
                belongsToBatch = true;
                item.Categories.Add(CreateCategory(typeof(Batch).Name, "Processed"));
            }
            if (batch.InvalidDetails.Contains(batchDetail))
            {
                belongsToBatch = true;
                item.Categories.Add(CreateCategory(typeof(Batch).Name, "Invalid"));
            }

            if (!belongsToBatch)
            {
                throw new ArgumentException("BatchDetail instance does not belong to injected Batch instance.");
            }

            // self link
            IDictionary<string, string> parameters = GetReplacementDictionary(entryId, workSummary.EndTime);
            item.Links.Add(CreateEntrySelfLink(parameters));

            // content
            item.Content = CreateContent(batchDetail);

            // add item to feed
            ((IList<SyndicationItem>)GetFeed().Items).Insert(0, item);

            return entryId;
        }
    }
}
