﻿using Saturn.Core.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Feed
{
    public interface IFeedParser
    {
        Uri GetFeedUri();
        IDictionary<string, string> GetResponseDictionary(HttpResult httpResult);
        void ProcessFeed(Uri feedUri);
    }
}
