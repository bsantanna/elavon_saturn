﻿using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel.Syndication;

namespace Saturn.Worker.Feed
{
    public abstract class AbstractWorkerFeed<T> : AbstractFeed<T>
    {
        protected readonly IWorkerContext workerContext;

        protected readonly IFeedRepository<T> feedRepository;

        private readonly SyndicationFeed workingFeed;

        private readonly string parentId;

        public AbstractWorkerFeed(IWorkerContext workerContext, IFeedRepository<T> feedRepository, UriTemplate feedTemplate, UriTemplate entryTemplate, string parentId)
            : base(new Uri(workerContext.FeedBaseUri()), feedTemplate, entryTemplate)
        {
            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;

            if (feedRepository == null)
            {
                throw new ArgumentNullException("feedRepository");
            }
            this.feedRepository = feedRepository;

            // nullable parent id
            this.parentId = parentId;

            // load working feed from repository
            this.workingFeed = feedRepository.GetWorkingFeed(baseAddress, feedTemplate, parentId);
        }

        protected void AddElementExtension<X>(X obj)
        {
            this.workingFeed.ElementExtensions.Add(obj, new DataContractSerializer(typeof(X)));
        }

        public override void Store()
        {
            string workingFeedPath = this.feedRepository.GetWorkingFeedPath(parentId);
            this.feedRepository.StoreWorkingFeed(this.workingFeed, workingFeedPath);
        }

        protected override SyndicationFeed GetFeed()
        {
            return this.workingFeed;
        }

        protected override string GetContentType()
        {
            return "application/vnd.elavon_worker_" + this.workerContext.WorkerName().ToLower() + "+xml";
        }

        protected IDictionary<string, string> GetReplacementDictionary(string entryId, DateTime dateTime)
        {
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");

            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("id", entryId);
            parameters.Add("y", year);
            parameters.Add("m", month);
            parameters.Add("d", day);

            return parameters;
        }
    }
}
