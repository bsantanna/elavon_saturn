﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Saturn.Worker.Common;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Feed
{
    public class WorkerFeedInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes
                    .FromAssemblyNamed("WorkerFeed")
                    .Where(t => t.Name.EndsWith("Feed"))
                    .WithServiceAllInterfaces()
            );

            container.Register(
                Component
                    .For<IWorker>()
                    .ImplementedBy<FeedWriter>()
                    .DependsOn(new Dependency[]{
                        Dependency.OnComponent("terminateWorker", typeof(TerminateWorker))
                    })
            );
        }
    }
}
