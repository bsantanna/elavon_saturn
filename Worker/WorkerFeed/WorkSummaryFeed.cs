﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel.Syndication;
using System.Text;

namespace Saturn.Worker.Feed
{
    public class WorkSummaryFeed : AbstractWorkerFeed<WorkSummary>
    {
        public static readonly string RecentFeedXml = "work_summary.xml";

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public WorkSummaryFeed(IWorkerContext workerContext, IFeedRepository<WorkSummary> feedRepository, IWorkSummaryRepository workSummaryRepository)
            : base(workerContext, feedRepository, new UriTemplate("/{y}/{m}/{d}/feed.xml"), new UriTemplate("/{y}/{m}/{d}/{id}/feed.xml"), null)
        {
            log.Debug("Creating instance...");
        }


        public override string AddEntry(WorkSummary workSummary)
        {
            // verifies if this workSummary belongs to working feed
            TimeSpan difference = TimeProvider.Current.UtcNow - workSummary.EndTime;
            if (difference.Days != 0)
            {
                throw new ArgumentException("WorkSummary EndTime does not belong to Current Feed.");
            }

            string entryId = GetId(workSummary);

            SyndicationItem item = new SyndicationItem
            {
                Id = entryId,
                Title = SyndicationContent.CreatePlaintextContent(entryId),
                LastUpdatedTime = workSummary.EndTime
            };

            // self link
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("id", entryId);
            parameters.Add("y", workSummary.EndTime.ToString("yyyy"));
            parameters.Add("m", workSummary.EndTime.ToString("MM"));
            parameters.Add("d", workSummary.EndTime.ToString("dd"));
            item.Links.Add(CreateEntrySelfLink(parameters));

            // category
            item.Categories.Add(CreateCategory(typeof(IWorker).Name, workSummary.WorkerName));

            // content
            item.Content = CreateContent(workSummary);

            // add item to feed
            ((IList<SyndicationItem>)GetFeed().Items).Insert(0, item);

            return entryId;
        }

        protected override SyndicationContent CreateContent(WorkSummary workSummary)
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("Work Summary");
            strBuilder.Append("<ul>");

            // work summary feed uri in content (for human navigation)
            IDictionary<string, string> parameters = GetReplacementDictionary(GetId(workSummary), workSummary.EndTime);
            Uri entryAddress = this.entryTemplate.BindByName(baseAddress, parameters);
            strBuilder.Append("<li>Entry feed: <a href='" + entryAddress.ToString() + "'>" + entryAddress.ToString() + "</a></li>");

            // work summary overview information
            strBuilder.Append("<li>Processed batch count: " + workSummary.ProcessedBatches.Count + "</li>");
            strBuilder.Append("<li>Pending batch count: " + workSummary.PendingBatches.Count + "</li>");
            strBuilder.Append("<li>Invalid batch count: " + workSummary.InvalidBatches.Count + "</li>");
            strBuilder.Append("</ul>");

            if (workSummary.ProcessedBatches.Count > 0)
            {
                strBuilder.Append("Processed Batch IDs: ");
                strBuilder.Append("<ul>");
                foreach (Batch batch in workSummary.ProcessedBatches)
                {
                    strBuilder.Append("<li>" + batch.BatchId + "</li>");
                }
                strBuilder.Append("</ul>");
            }

            if (workSummary.PendingBatches.Count > 0)
            {
                strBuilder.Append("Pending Batch IDs: ");
                strBuilder.Append("<ul>");
                foreach (Batch batch in workSummary.PendingBatches)
                {
                    strBuilder.Append("<li>" + batch.BatchId + "</li>");
                }
                strBuilder.Append("</ul>");
            }

            if (workSummary.InvalidBatches.Count > 0)
            {
                strBuilder.Append("Invalid Batch IDs: ");
                strBuilder.Append("<ul>");
                foreach (Batch batch in workSummary.InvalidBatches)
                {
                    strBuilder.Append("<li>" + batch.BatchId + "</li>");
                }
                strBuilder.Append("</ul>");
            }

            return SyndicationContent.CreateHtmlContent(strBuilder.ToString());
        }
    }
}
