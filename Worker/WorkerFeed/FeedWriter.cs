﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Feed
{
    public class FeedWriter : AbstractWorker
    {
        private readonly IFeed<WorkSummary> workSummaryFeed;

        private readonly IFeedRepository<Batch> batchFeedRepository;

        private readonly IFeedRepository<BatchDetail> batchDetailFeedRepository;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FeedWriter(IWorkerContext workerContext, IMailService mailService, IBatchService batchService, IWorker terminateWorker, IFeed<WorkSummary> workSummaryFeed, IFeedRepository<Batch> batchFeedRepository, IFeedRepository<BatchDetail> batchDetailFeedRepository)
            : base(workerContext, mailService, batchService, terminateWorker)
        {
            log.Debug("Creating instance...");

            if (workSummaryFeed == null)
            {
                throw new ArgumentNullException("workSummaryFeed");
            }
            this.workSummaryFeed = workSummaryFeed;

            if (batchFeedRepository == null)
            {
                throw new ArgumentNullException("batchFeedRepository");
            }
            this.batchFeedRepository = batchFeedRepository;

            if (batchDetailFeedRepository == null)
            {
                throw new ArgumentNullException("batchDetailFeedRepository");
            }
            this.batchDetailFeedRepository = batchDetailFeedRepository;
        }

        public override void Work(WorkSummary workSummary)
        {
            // Create a new entry for work summary feed
            string workSummaryFeedId = workSummaryFeed.AddEntry(workSummary);

            // Create a new batch feed for corresponding workSummary
            IFeed<Batch> batchFeed = new BatchFeed(workerContext, batchFeedRepository, workSummaryFeedId, workSummary);

            // Iterate over all batches
            IEnumerable<Batch> allBatches = workSummary.ProcessedBatches.Union(workSummary.PendingBatches.Union(workSummary.InvalidBatches));
            foreach (Batch batch in allBatches)
            {

                // add entry to batch feed
                string batchFeedId = batchFeed.AddEntry(batch);

                // create a new batch detail feed for corresponding batch
                IFeed<BatchDetail> batchDetailFeed = new BatchDetailFeed(workerContext, batchDetailFeedRepository, workSummaryFeedId, workSummary, batchFeedId, batch);

                // Iterate over all details
                IEnumerable<BatchDetail> allDetails = batch.Details.Union(batch.InvalidDetails);
                foreach (BatchDetail batchDetail in allDetails)
                {
                    batchDetailFeed.AddEntry(batchDetail);
                }

                // store batch details feed
                batchDetailFeed.Store();
            }

            // store batch feed
            batchFeed.Store();

            // store work summary feed
            workSummaryFeed.Store();
        }
    }
}
