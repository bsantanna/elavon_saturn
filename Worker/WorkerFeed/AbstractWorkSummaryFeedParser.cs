﻿using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Linq;
using System.Timers;
using System.Text.RegularExpressions;
using log4net;

namespace Saturn.Worker.Feed
{
    public abstract class AbstractWorkSummaryFeedParser : IFeedParser
    {
        protected readonly IWorkerContext workerContext;
        private ISet<string> readItems;
        private readonly Timer resetReadItemsTimer;
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected readonly XmlReaderSettings xmlReaderSettings = new XmlReaderSettings { CloseInput = true };

        public AbstractWorkSummaryFeedParser(IWorkerContext workerContext)
        {
            log.Debug("Creating instance...");

            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;

            readItems = new HashSet<string>();

            int interval = 86400; // 1 day
            this.resetReadItemsTimer = new Timer();
            this.resetReadItemsTimer.Elapsed += new ElapsedEventHandler(TimerCallback);
            this.resetReadItemsTimer.Interval = interval * 1000;
            this.resetReadItemsTimer.Enabled = true;
        }

        public Uri GetFeedUri()
        {
            return new Uri(workerContext.FeedBaseUri() + "/" + WorkSummaryFeed.RecentFeedXml);
        }

        public IDictionary<string, string> GetResponseDictionary(HttpResult httpResult)
        {
            IDictionary<string, string> dictionary = new Dictionary<string, string>();

            string[] responsePairs = Regex.Split(httpResult.Response, "\r{0,1}\n");
            foreach (string pair in responsePairs)
            {
                if (!String.IsNullOrEmpty(pair))
                {
                    int index = pair.IndexOf('=');
                    dictionary.Add(pair.Substring(0, index), pair.Substring(index + 1));
                }
            }

            return dictionary;
        }

        protected SyndicationFeed LoadFeed(Uri uri)
        {
            // load feed from web
            SyndicationFeed feed = null;
            WebRequest request = WebRequest.Create(uri);
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (XmlReader xmlReader = XmlReader.Create(stream, xmlReaderSettings))
                    {
                        feed = SyndicationFeed.Load(xmlReader);
                    }
                }
            }

            return feed;
        }

        protected abstract void ProcessBatchDetail(BatchDetail batchDetail, bool batchDetailIsValid);

        protected abstract void ProcessBatchDetailFeed(SyndicationFeed batchDetailFeed, bool batchIsValid);

        protected abstract void ProcessBatchFeed(SyndicationFeed batchFeed);

        public void ProcessFeed(Uri feedUri)
        {
            SyndicationFeed workSummaryRecentFeed = LoadFeed(feedUri);
            Queue<SyndicationItem> unreadItems = new Queue<SyndicationItem>();
            foreach (SyndicationItem workSummaryItem in workSummaryRecentFeed.Items)
            {
                if (!readItems.Contains(workSummaryItem.Id))
                {
                    unreadItems.Enqueue(workSummaryItem);
                }
            }

            while (unreadItems.Count > 0)
            {
                SyndicationItem currentWorkSummaryItem = unreadItems.Peek();
                Uri batchFeedUri = currentWorkSummaryItem.Links.First(x => x.RelationshipType.Equals("self")).Uri;
                SyndicationFeed batchFeed = LoadFeed(batchFeedUri);
                foreach (SyndicationItem batchItem in batchFeed.Items)
                {
                    Uri batchDetailFeedUri = batchItem.Links.First(x => x.RelationshipType.Equals("self")).Uri;
                    SyndicationFeed batchDetailFeed = LoadFeed(batchDetailFeedUri);
                    foreach (SyndicationItem batchDetailItem in batchDetailFeed.Items)
                    {
                        SyndicationCategory batchCategory = batchDetailItem.Categories.Single(cat => cat.Scheme.EndsWith("Batch"));
                        bool batchDetailIsValid = !batchCategory.Name.Equals("Invalid");
                        XmlSyndicationContent content = batchDetailItem.Content as XmlSyndicationContent;
                        ProcessBatchDetail(content.ReadContent<BatchDetail>(), batchDetailIsValid);
                    }
                    SyndicationCategory workSummaryCategory = batchItem.Categories.Single(cat => cat.Scheme.EndsWith("WorkSummary"));
                    bool batchIsValid = !workSummaryCategory.Name.Equals("Invalid");
                    ProcessBatchDetailFeed(batchDetailFeed, batchIsValid);
                }
                ProcessBatchFeed(batchFeed);
                readItems.Add(unreadItems.Dequeue().Id);
            }
        }

        private void TimerCallback(object source, ElapsedEventArgs e)
        {
            this.readItems.Clear();
        }
    }
}
