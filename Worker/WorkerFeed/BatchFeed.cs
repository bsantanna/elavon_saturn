﻿using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.ServiceModel.Syndication;
using System.Text;

namespace Saturn.Worker.Feed
{
    public class BatchFeed : AbstractWorkerFeed<Batch>
    {
        private readonly string workSummaryFeedId;

        private readonly WorkSummary workSummary;

        public BatchFeed(IWorkerContext workerContext, IFeedRepository<Batch> feedRepository, string workSummaryFeedId, WorkSummary workSummary)
            : base(workerContext, feedRepository, new UriTemplate("/{y}/{m}/{d}/" + workSummaryFeedId + "/feed.xml"), new UriTemplate("/{y}/{m}/{d}/" + workSummaryFeedId + "/{id}/feed.xml"), workSummaryFeedId)
        {
            if (workSummaryFeedId == null)
            {
                throw new ArgumentNullException("workSummaryFeedId");
            }
            this.workSummaryFeedId = workSummaryFeedId;

            if (workSummary == null)
            {
                throw new ArgumentNullException("workSummary");
            }
            this.workSummary = workSummary;
        }

        public override string AddEntry(Batch batch)
        {
            string entryId = GetId(batch);

            SyndicationItem item = new SyndicationItem
            {
                Id = entryId,
                Title = SyndicationContent.CreatePlaintextContent(workSummaryFeedId + " => " + entryId),
                LastUpdatedTime = workSummary.EndTime
            };
            
            // category
            item.Categories.Add(CreateCategory(typeof(IWorker).Name, workSummary.WorkerName));
            bool belongsToWorkSummary = false;
            if (workSummary.ProcessedBatches.Contains(batch))
            {
                belongsToWorkSummary = true;
                item.Categories.Add(CreateCategory(typeof(WorkSummary).Name, "Processed"));
            }

            if (workSummary.PendingBatches.Contains(batch))
            {
                belongsToWorkSummary = true;
                item.Categories.Add(CreateCategory(typeof(WorkSummary).Name, "Pending"));
            }

            if (workSummary.InvalidBatches.Contains(batch))
            {
                belongsToWorkSummary = true;
                item.Categories.Add(CreateCategory(typeof(WorkSummary).Name, "Invalid"));
            }

            if (!belongsToWorkSummary)
            {
                throw new ArgumentException("Batch does not belong to injected WorkSummary instance.");
            }

            // self link
            IDictionary<string, string> parameters = GetReplacementDictionary(entryId, workSummary.EndTime);
            item.Links.Add(CreateEntrySelfLink(parameters));

            // content
            item.Content = CreateContent(batch);

            // add item to feed
            ((IList<SyndicationItem>)GetFeed().Items).Insert(0, item);

            return entryId;
        }

        protected override SyndicationContent CreateContent(Batch batch)
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("Batch Info:");
            strBuilder.Append("<ul>");

            // batch feed uri in content (for human navigation)
            IDictionary<string, string> parameters = GetReplacementDictionary(GetId(batch), workSummary.EndTime);
            Uri entryAddress = this.entryTemplate.BindByName(baseAddress, parameters);
            strBuilder.Append("<li>Entry feed: <a href='" + entryAddress.ToString() + "'>" + entryAddress.ToString() + "</a></li>");

            // batch generay info
            strBuilder.Append("<li>Batch ID: " + batch.BatchId + "</li>");
            strBuilder.Append("<li>Terminal ID: " + batch.TerminalId + "</li>");
            strBuilder.Append("<li>Total amount: " + batch.GetTotalAmount() + "</li>");
            strBuilder.Append("<li>Transaction count: " + batch.Details.Count + "</li>");
            strBuilder.Append("<li>Invalid transaction count: " + batch.InvalidDetails.Count + "</li>");
            strBuilder.Append("</ul>");

            if (batch.HeaderHttpResult != null)
            {
                strBuilder.Append("Header Request Log:");
                strBuilder.Append("<ul>");
                strBuilder.Append("<li>Time: " + batch.HeaderHttpResult.EndTime.ToString() + "</li>");
                strBuilder.Append("<li>Request: " + batch.HeaderHttpResult.Request + "</li>");
                strBuilder.Append("<li>Response: " + batch.HeaderHttpResult.Response + "</li>");
                strBuilder.Append("</ul>");
            }

            if (batch.TrailerHttpResult != null)
            {
                strBuilder.Append("Trailer Request Log:");
                strBuilder.Append("<ul>");
                strBuilder.Append("<li>Time: " + batch.TrailerHttpResult.EndTime.ToString() + "</li>");
                strBuilder.Append("<li>Request: " + batch.TrailerHttpResult.Request + "</li>");
                strBuilder.Append("<li>Response: " + batch.TrailerHttpResult.Response + "</li>");
                strBuilder.Append("</ul>");
            }

            if (batch.Details.Count > 0)
            {
                strBuilder.Append("Transaction IDs:");
                strBuilder.Append("<ul>");
                foreach (BatchDetail detail in batch.Details)
                {
                    strBuilder.Append("<li>" + detail.TransactionID + "</li>");
                }
                strBuilder.Append("</ul>");
            }

            if (batch.InvalidDetails.Count > 0)
            {
                strBuilder.Append("Invalid transaction IDs:");
                strBuilder.Append("<ul>");
                foreach (BatchDetail detail in batch.InvalidDetails)
                {
                    strBuilder.Append("<li>" + detail.TransactionID + "</li>");
                }
                strBuilder.Append("</ul>");
            }

            return SyndicationContent.CreateHtmlContent(strBuilder.ToString());
        }
    }
}
