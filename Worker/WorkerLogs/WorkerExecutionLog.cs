﻿using Castle.DynamicProxy;
using log4net;
using Saturn.Worker.Domain;
using System;

namespace Saturn.Worker.Logs
{
    public class WorkerExecutionLog : IInterceptor
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Intercept(IInvocation invocation)
        {
            IWorker worker = invocation.InvocationTarget as IWorker;

            string prefix = "IWorker => " + worker.GetType().Name;
            if ("Work".Equals(invocation.Method.Name))
            {
                log.Info(prefix + " => " + invocation.Method.Name + "() starting");
            }
            
            invocation.Proceed();

            string suffix = "";

            Type argumentType = null;

            if (invocation.Arguments != null && invocation.Arguments.Length > 0)
            {
                argumentType = invocation.Arguments[0].GetType();
            }

            if (argumentType != null && argumentType.Equals(typeof(WorkSummary)))
            {
                WorkSummary workSummary = (WorkSummary)invocation.Arguments[0];
                suffix += "result: Invalid[" + workSummary.InvalidBatches.Count + "], Pending[" + workSummary.PendingBatches.Count + "], Processed[" + workSummary.ProcessedBatches.Count + "]";
            }
            if ("Work".Equals(invocation.Method.Name))
            {
                log.Info(prefix + " <= " + invocation.Method.Name + "() " + suffix);
            }
        }
    }
}
