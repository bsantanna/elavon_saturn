﻿using Castle.DynamicProxy;
using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Logs
{
    public class BatchServiceAccessLog : IInterceptor
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();

                if ("GetBatches".Equals(invocation.Method.Name))
                {
                    ICollection<Batch> batches = invocation.ReturnValue as ICollection<Batch>;
                    log.Debug("Recovered " + batches.Count + " batches from IBatchService");
                }
                else if ("GetPendingWork".Equals(invocation.Method.Name))
                {
                    ICollection<WorkSummary> workSummary = invocation.ReturnValue as ICollection<WorkSummary>;
                    log.Debug("Recovered " + workSummary.Count + " pending work from IBatchService");
                }
                else if ("StoreWorkSummary".Equals(invocation.Method.Name))
                {
                    string path = invocation.ReturnValue as string;
                    log.Debug("Stored WorkSummary at " + path);
                }
            }
            catch (Exception e)
            {
                log.Error("While calling " + invocation.Method.Name + "()");
                throw;
            }
        }
    }
}
