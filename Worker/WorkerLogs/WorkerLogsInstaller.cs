﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Logs
{
    public class WorkerLogsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<BatchServiceAccessLog>());
            container.Register(Component.For<WorkerExecutionLog>());
        }
    }
}
