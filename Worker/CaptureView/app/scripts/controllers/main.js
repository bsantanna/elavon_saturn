'use strict';

/**
 * @ngdoc function
 * @name captureViewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the captureViewApp
 */
angular.module('captureViewApp')
  .controller('MainCtrl', function ($scope, feedService) {
    $scope.placeholder = '';
    feedService.loadFeed('http://dailyjs.com/atom.xml').then(function(data){
      console.log(data);
    });
  });
