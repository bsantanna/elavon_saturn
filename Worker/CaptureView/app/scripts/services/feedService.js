'use strict';
/**
 * Created by bsantanna on 29/12/14.
 */

captureViewApp.factory('feedLoader', function ($resource) {
  return $resource('http://ajax.googleapis.com/ajax/services/feed/load', {}, {
    fetch: {method: 'JSONP', params: {v: '1.0', callback: 'JSON_CALLBACK'}}
  });
});

captureViewApp.service('feedService', function ($rootScope, feedLoader, $q) {
  this.loadFeed = function (feedUri) {
    var deffered = $q.defer();
    feedLoader.fetch({q: feedUri}, {}, function (data) {
      deffered.resolve(data.responseData.feed);
    });
    return deffered.promise;
  };
});
