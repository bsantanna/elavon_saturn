'use strict';

/**
 * @ngdoc overview
 * @name captureViewApp
 * @description
 * # captureViewApp
 *
 * Main module of the application.
 */
var captureViewApp = angular
  .module('captureViewApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
