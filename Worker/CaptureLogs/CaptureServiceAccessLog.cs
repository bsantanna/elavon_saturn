﻿using Castle.DynamicProxy;
using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;

namespace Saturn.Worker.Capture.Logs
{
    public class CaptureServiceAccessLog : IInterceptor
    {
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private DateTime lastInterception;
        private int detailCount;

        private int GetDeltaTimeMillis()
        {
            TimeSpan difference = TimeProvider.Current.UtcNow - lastInterception;
            return difference.Milliseconds;
        }

        public void Intercept(IInvocation invocation)
        {
            invocation.Proceed();

            bool isValid = true;
            string invalidMessage = "";

            if ("IsBatchValid".Equals(invocation.Method.Name))
            {
                isValid = (bool)invocation.ReturnValue;
                invalidMessage = "Batch is marked as invalid, probably already processed.";
                log.Debug(invocation.Method.Name + "() => " + invocation.ReturnValue);
            }
            else if ("IsHeaderValid".Equals(invocation.Method.Name))
            {
                isValid = (bool)invocation.ReturnValue;
                detailCount = 0;
                invalidMessage = "Batch Header response is evaluated as invalid, batch added to WorkSummary Invalid Set.";
                log.Debug(invocation.Method.Name + "() => " + invocation.ReturnValue + ", Request time => " + GetDeltaTimeMillis() + "ms");
            }
            else if ("IsDetailValid".Equals(invocation.Method.Name))
            {
                isValid = (bool)invocation.ReturnValue;
                detailCount++;
                invalidMessage = "Batch Detail response is evaluated as invalid, batch will be fixed and enqueued again.";
                log.Debug(invocation.Method.Name + "() => " + invocation.ReturnValue + ", Request time => " + GetDeltaTimeMillis() + "ms, Detail count => " + detailCount);
            }
            else if ("IsTrailerValid".Equals(invocation.Method.Name))
            {
                isValid = (bool)invocation.ReturnValue;
                detailCount = 0;
                invalidMessage = "Batch Trailer response is evaluated as invalid, batch added to WorkSummary Invalid Set.";
                log.Debug(invocation.Method.Name + "() => " + invocation.ReturnValue + ", Request time => " + GetDeltaTimeMillis() + "ms");
            }
            else if ("GetHeader".Equals(invocation.Method.Name))
            {
                Batch batch = invocation.Arguments[0] as Batch;
                log.Debug("Sending Header for Batch ID => " + batch.BatchId + ", Terminal ID => " + batch.TerminalId + ", Total number of records => " + (batch.Details.Count + 2));
            }
            else if ("GetDetail".Equals(invocation.Method.Name))
            {
                Batch batch = invocation.Arguments[0] as Batch;
                BatchDetail batchDetail = invocation.Arguments[1] as BatchDetail;
                log.Debug("Sending Detail for Batch ID => " + batch.BatchId + ", Terminal ID => " + batch.TerminalId + ", Transaction ID => " + batchDetail.TransactionID);
            }
            else if ("GetTrailer".Equals(invocation.Method.Name))
            {
                Batch batch = invocation.Arguments[0] as Batch;
                log.Debug("Sending Trailer for Batch ID => " + batch.BatchId + ", Terminal ID => " + batch.TerminalId + ", Total number of records => " + (batch.Details.Count + 2));
            }

            if (!isValid)
            {
                log.Warn(invalidMessage);
            }

            lastInterception = TimeProvider.Current.UtcNow;
        }
    }
}
