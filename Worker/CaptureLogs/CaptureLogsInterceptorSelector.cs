﻿using Castle.Core;
using Castle.MicroKernel.Proxy;

namespace Saturn.Worker.Capture.Logs
{
    public class CaptureLogsInterceptorSelector : IModelInterceptorsSelector
    {

        public bool HasInterceptors(ComponentModel model)
        {
            return IsICaptureServiceInstance(model);
        }

        public InterceptorReference[] SelectInterceptors(ComponentModel model, InterceptorReference[] interceptors)
        {
            InterceptorReference[] selectedInterceptors = null;

            if (IsICaptureServiceInstance(model))
            {
                selectedInterceptors = new InterceptorReference[] {
                    InterceptorReference.ForType<CaptureServiceAccessLog>(),
                };
            }
            else
            {
                selectedInterceptors = new InterceptorReference[] { };
            }

            return selectedInterceptors;
        }

        private bool IsICaptureServiceInstance(ComponentModel model)
        {
            return model.Implementation.Name.Equals("CaptureService");
        }

    }
}
