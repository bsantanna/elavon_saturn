﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;

namespace Saturn.Worker.Capture.Logs
{
    public class CaptureLogsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<CaptureServiceAccessLog>());
            container.Kernel.ProxyFactory.AddInterceptorSelector(new CaptureLogsInterceptorSelector());
        }
    }
}
