﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel.Syndication;
using System.Text;
using System.Xml;

namespace Saturn.Worker.Data.Binary
{
    public abstract class AbstractFeedRepository<T> : AbstractFileRepository<T>, IFeedRepository<T>
    {
        protected readonly XmlReaderSettings xmlReaderSettings = new XmlReaderSettings { CloseInput = true };
        protected readonly XmlWriterSettings xmlWriterSettings = new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true, CloseOutput = true };
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AbstractFeedRepository(IWorkerContext workerContext) : base(workerContext) {
            log.Debug("Creating instance...");
        }

        public string ArchiveFeed(SyndicationFeed archive, Uri baseAddress, UriTemplate feedTemplate, DateTime archiveDate)
        {
            archive.GetSelfLink().Uri = archive.GetViaLink().Uri;
            archive.Links.Remove(archive.GetViaLink());
            archive.Links.Add(new SyndicationLink() { RelationshipType = "next-archive", Uri = feedTemplate.BindByName(baseAddress, GetReplacementDictionary(TimeProvider.Current.UtcNow)) });
            archive.ElementExtensions.Add(new SyndicationElementExtension("archive", "http://purl.org/syndication/history/1.0", string.Empty));

            string feedPath = GetFeedPath(archiveDate);
            StoreWorkingFeed(archive, feedPath);

            return feedPath;
        }

        public virtual SyndicationFeed GetWorkingFeed(Uri baseAddress, UriTemplate feedTemplate, string parentId)
        {
            string workingFeedPath = GetWorkingFeedPath(parentId);

            SyndicationFeed workingFeed = null;
            if (File.Exists(workingFeedPath))
            {
                using (XmlReader reader = XmlReader.Create(workingFeedPath, xmlReaderSettings))
                {
                    workingFeed = SyndicationFeed.Load(reader);
                }
            }
            else
            {
                string title = string.IsNullOrEmpty(parentId) ? typeof(T).Name : parentId + " => " + typeof(T).Name;
                workingFeed = new SyndicationFeed
                {
                    Id = new UniqueId(Guid.NewGuid()).ToString(),
                    Title = SyndicationContent.CreatePlaintextContent(title),
                    Generator = workerContext.WorkerName(),
                    LastUpdatedTime = TimeProvider.Current.UtcNow,
                    Items = new List<SyndicationItem>()
                };

                workingFeed.Authors.Add(new SyndicationPerson { Name = workerContext.WorkerName() });

                // via link
                workingFeed.Links.Add(new SyndicationLink() { RelationshipType = "via", Uri = feedTemplate.BindByName(baseAddress, GetReplacementDictionary(TimeProvider.Current.UtcNow)) });

                // store working feed
                StoreWorkingFeed(workingFeed, workingFeedPath);
            }

            return workingFeed;
        }

        public string GetWorkingFeedPath(string parentId)
        {
            string path = GetFeedPath(TimeProvider.Current.UtcNow);
            if (!string.IsNullOrEmpty(parentId))
            {
                string prefix = path.Substring(0, path.LastIndexOf("\\"));
                string suffix = path.Substring(path.LastIndexOf("\\"));
                path = prefix + "\\" + parentId + suffix;
            }
            return path;
        }

        protected virtual string GetFeedPath(DateTime dateTime)
        {
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");

            return Path.Combine(GetWorkingDirectory(), year, month, day, "feed" + GetFileExtension());
        }

        protected IDictionary<string, string> GetReplacementDictionary(DateTime dateTime)
        {
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");

            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("y", year);
            parameters.Add("m", month);
            parameters.Add("d", day);

            return parameters;
        }

        protected override string GetFileExtension()
        {
            return ".xml";
        }

        public virtual void StoreWorkingFeed(SyndicationFeed feed, string feedPath)
        {
            // set last updated time
            feed.LastUpdatedTime = TimeProvider.Current.UtcNow;

            string directory = feedPath.Substring(0, feedPath.LastIndexOf("\\"));
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (XmlWriter writer = XmlWriter.Create(feedPath, xmlWriterSettings))
            {
                feed.GetAtom10Formatter().WriteTo(writer);
            }
        }
    }
}
