﻿using Saturn.Worker.Domain;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Saturn.Worker.Data.Sql
{
    public class SqlBackOfficeRepository : AbstractSqlRepository, IBackOfficeRepository
    {

        public SqlBackOfficeRepository(IWorkerContext workerContext) : base(workerContext) { }

        public void LogTrans(string terminalId, string transactionId, string paymentTransaction, string source, string responseCode, string responseMessage, string dumpRequest, string dumpResponse, string sourceIpAddress, DateTime dateTimeRequest, DateTime dateTimeResponse)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_LOGTRANS_INSERT", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("TransactionID", SqlDbType.NVarChar, transactionId));
                command.Parameters.Add(GetParameter("TerminalID", SqlDbType.NVarChar, terminalId));
                command.Parameters.Add(GetParameter("PaymentTransaction", SqlDbType.NVarChar, paymentTransaction));
                command.Parameters.Add(GetParameter("Source", SqlDbType.NVarChar, source));
                command.Parameters.Add(GetParameter("DateTimeRequest", SqlDbType.DateTimeOffset, (DateTimeOffset)dateTimeRequest));
                command.Parameters.Add(GetParameter("DateTimeResponse", SqlDbType.DateTimeOffset, (DateTimeOffset)dateTimeResponse));
                command.Parameters.Add(GetParameter("ResponseCode", SqlDbType.NVarChar, responseCode));
                command.Parameters.Add(GetParameter("ResponseMessage", SqlDbType.NVarChar, responseMessage));
                command.Parameters.Add(GetParameter("DumpRequest", SqlDbType.NVarChar, dumpRequest));
                command.Parameters.Add(GetParameter("DumpResponse", SqlDbType.NVarChar, dumpResponse));
                command.Parameters.Add(GetParameter("SourceIPAddress", SqlDbType.NVarChar, sourceIpAddress));
                
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void LogTasker(string terminalId, string status, string paymentTransaction, string responseDetails, string recordCount, DateTime taskerDateTime)
        {
            LogTasker(terminalId, null, status, paymentTransaction, responseDetails, recordCount, taskerDateTime);
        }

        public void LogTasker(string terminalId, string transactionId, string status, string paymentTransaction, string responseDetails, string recordCount, DateTime taskerDateTime)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_LOGTASKER_INSERT", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("Status", SqlDbType.NVarChar, status));
                command.Parameters.Add(GetParameter("PaymentTransaction", SqlDbType.NVarChar, paymentTransaction));
                command.Parameters.Add(GetParameter("TerminalID", SqlDbType.NVarChar, terminalId));
                command.Parameters.Add(GetParameter("TransactionID", SqlDbType.NVarChar, transactionId));
                command.Parameters.Add(GetParameter("ResponseDetails", SqlDbType.NVarChar, responseDetails));
                command.Parameters.Add(GetParameter("TaskerDateTime", SqlDbType.DateTimeOffset, taskerDateTime));
                command.Parameters.Add(GetParameter("RecordCount", SqlDbType.Int, recordCount));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void UpdateHeader(string batchId, string terminalId, string dumpHeaderRequest, string dumpHeaderResponse)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_TASKERCAPTURE_HEADER_UPDATE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("BatchID", SqlDbType.BigInt, batchId));
                command.Parameters.Add(GetParameter("TerminalID", SqlDbType.NVarChar, terminalId));
                command.Parameters.Add(GetParameter("DumpHeaderRequest", SqlDbType.NVarChar, dumpHeaderRequest));
                command.Parameters.Add(GetParameter("DumpHeaderResponse", SqlDbType.NVarChar, dumpHeaderResponse));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void UpdateDetail(string batchId, string terminalId, string transactionId, string dumpDetailRequest, string dumpDetailResponse)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_TASKERCAPTURE_DETAIL_UPDATE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("BatchID", SqlDbType.BigInt, batchId));
                command.Parameters.Add(GetParameter("TerminalID", SqlDbType.NVarChar, terminalId));
                command.Parameters.Add(GetParameter("TransactionID", SqlDbType.NVarChar, transactionId));
                command.Parameters.Add(GetParameter("DumpDetailRequest", SqlDbType.NVarChar, dumpDetailRequest));
                command.Parameters.Add(GetParameter("DumpDetailResponse", SqlDbType.NVarChar, dumpDetailResponse));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void UpdateTrailer(string batchId, string terminalId, string dumpTrailerRequest, string dumpTrailerResponse)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_TASKERCAPTURE_TRAILER_UPDATE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("BatchID", SqlDbType.BigInt, batchId));
                command.Parameters.Add(GetParameter("TerminalID", SqlDbType.NVarChar, terminalId));
                command.Parameters.Add(GetParameter("DumpTrailerRequest", SqlDbType.NVarChar, dumpTrailerRequest));
                command.Parameters.Add(GetParameter("DumpTrailerResponse", SqlDbType.NVarChar, dumpTrailerResponse));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
