﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using Saturn.Worker.Feed;
using System;
using System.IO;
using System.ServiceModel.Syndication;
using System.Xml;

namespace Saturn.Worker.Data.Binary
{
    public class FileWorkSummaryFeedRepository : AbstractFeedRepository<WorkSummary>
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FileWorkSummaryFeedRepository(IWorkerContext workerContext) : base(workerContext) {
            log.Debug("Creating instance...");
        }

        public override SyndicationFeed GetWorkingFeed(Uri baseAddress, UriTemplate feedTemplate, string parentId)
        {
            string workingFeedPath = GetWorkingFeedPath(parentId);
            bool isNewFeed = !File.Exists(workingFeedPath);
            SyndicationFeed workingFeed = base.GetWorkingFeed(baseAddress, feedTemplate, parentId);

            if (isNewFeed)
            {
                // self link
                UriTemplate recentTemplate = new UriTemplate("/" + WorkSummaryFeed.RecentFeedXml);
                workingFeed.Links.Add(SyndicationLink.CreateSelfLink(recentTemplate.BindByPosition(baseAddress)));

                // prev-archive link
                // try 31 days in past
                bool found = false;
                for (int i = 1; i <= 31 && !found; i++)
                {
                    DateTime dayBefore = TimeProvider.Current.UtcNow.AddDays(i * -1);
                    string day = dayBefore.ToString("dd");
                    string month = dayBefore.ToString("MM");
                    string year = dayBefore.ToString("yyyy");
                    var archiveFeedPath = Path.Combine(GetWorkingDirectory(), year, month, day, "feed" + GetFileExtension());
                    if (File.Exists(archiveFeedPath))
                    {
                        found = true;

                        // create prev-archive link in current working feed
                        workingFeed.Links.Add(new SyndicationLink() { RelationshipType = "prev-archive", Uri = feedTemplate.BindByName(baseAddress, GetReplacementDictionary(dayBefore)) });

                        // archive previous working feed
                        SyndicationFeed previousWorkingFeed = null;
                        using (XmlReader reader = XmlReader.Create(archiveFeedPath, xmlReaderSettings))
                        {
                            previousWorkingFeed = SyndicationFeed.Load(reader);
                        }
                        ArchiveFeed(previousWorkingFeed, baseAddress, feedTemplate, dayBefore);
                    }
                }

                StoreWorkingFeed(workingFeed, workingFeedPath);
            }

            return workingFeed;
        }

        protected override DateTime GetDateTime(WorkSummary workSummary)
        {
            return workSummary.EndTime;
        }

        public override void StoreWorkingFeed(SyndicationFeed feed, string feedPath)
        {
            // store feed
            base.StoreWorkingFeed(feed, feedPath);

            // create recent feed path
            string recentFeedPath = Path.Combine(GetWorkingDirectory(), WorkSummaryFeed.RecentFeedXml);

            // replicate feed to recent
            string directory = feedPath.Substring(0, feedPath.LastIndexOf("\\"));
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (XmlWriter writer = XmlWriter.Create(recentFeedPath, xmlWriterSettings))
            {
                feed.GetAtom10Formatter().WriteTo(writer);
            }
        }
    }
}
