﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace Saturn.Worker.Data.Sql
{
    public class SqlBatchRepository : AbstractSqlRepository, IBatchRepository
    {

        private readonly IXmlService xmlService;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SqlBatchRepository(IWorkerContext workerContext, IXmlService xmlService)
            : base(workerContext)
        {
            log.Debug("Creating instance...");

            if (xmlService == null)
            {
                throw new ArgumentNullException("xmlService");
            }
            this.xmlService = xmlService;
        }

        public ICollection<Batch> GetBatches()
        {
            Dictionary<string, Batch> batchDictionary = new Dictionary<string, Batch>();

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_TASKERCAPTURE_PENDING_SELECT", connection);
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                using (SqlDataReader batchDataReader = command.ExecuteReader())
                {
                    if (batchDataReader.HasRows)
                    {
                        while (batchDataReader.Read())
                        {
                            string batchId = GetValue(batchDataReader, "BatchID");

                            // recover batch from dictionary
                            if (!batchDictionary.ContainsKey(batchId))
                            {
                                Batch batch = new Batch();
                                batch.BatchId = batchId;
                                batch.TerminalId = GetValue(batchDataReader, "DynamicTerminalID");
                                if (String.IsNullOrEmpty(batch.TerminalId))
                                {
                                    batch.TerminalId = GetValue(batchDataReader, "TerminalID");
                                }
                                batch.Version = GetValue(batchDataReader, ("Version"));
                                batch.ApplicationId = GetValue(batchDataReader, "ApplicationID");
                                batch.BatchNumber = GetValue(batchDataReader, "BatchNumber");
                                batchDictionary.Add(batchId, batch);
                            }

                            // populate batchDetail 
                            BatchDetail batchDetail = new BatchDetail();

                            // data from columns
                            batchDetail.TransactionID = GetValue(batchDataReader, "TransactionID");
                            batchDetail.Amount = GetValue(batchDataReader, "Amount");
                            batchDetail.Request = GetValue(batchDataReader, "Request");
                            batchDetail.PurchaseDepartureDate = GetValue(batchDataReader, "PurchaseDepartureDate");
                            batchDetail.AuthorizationSource = GetValue(batchDataReader, "AuthorizationSource");
                            batchDetail.PS2000Data = GetValue(batchDataReader, "PS2000Data");
                            batchDetail.MSDI = GetValue(batchDataReader, "MSDI");
                            batchDetail.MerchantDynamicDBA = GetValue(batchDataReader, "MerchantDynamicDBA");
                            batchDetail.PurchaseAdditionalID = GetValue(batchDataReader, "PurchaseAdditionalID");
                            batchDetail.DebitEBTNetworkID = GetValue(batchDataReader, "DebitEBTNetworkID");
                            batchDetail.DebitEBTSettlementDate = GetValue(batchDataReader, "DebitEBTSettlementDate");
                            batchDetail.DebitInterchangeIndicator = GetValue(batchDataReader, "DebitInterchangeIndicator");

                            // data from xml documents
                            string xmlString = GetValue(batchDataReader, "PaymentRequestDetails");
                            XmlDocument paymentRequestDetailsXml = this.xmlService.GetXmlDocument(xmlString);
                            batchDetail.CardProduct = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/CardProduct");
                            batchDetail.ECI = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/ECI");
                            batchDetail.ThreeDSAuthenticationValue = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/AuthenticationDetails/ThreeDS/AuthenticationValue");
                            batchDetail.Recurring = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/Recurring");
                            batchDetail.InstallmentsRecurring = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/Installments/@type");
                            batchDetail.Installments = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/Installments");
                            batchDetail.CaptureDetailsInstallments = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentResponseDetails/Card[1]/CaptureDetails/Installments[1]");
                            batchDetail.CardData = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/CardData");
                            batchDetail.AuthAmount = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/AuthorizationAmount");
                            batchDetail.AuthAmountCurrencyCode = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/AuthorizationAmount/@currencyCode");
                            batchDetail.POSEntryCapability = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/POSEntryCapability");
                            batchDetail.CardEntryMode = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/CardEntryMode");
                            batchDetail.PINBlockDetails = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/PINBlockDetails");
                            batchDetail.ICCDetails = this.xmlService.GetValue(paymentRequestDetailsXml, "/PaymentRequestDetails/Card[1]/ICCDetails");

                            xmlString = GetValue(batchDataReader, "PaymentResponseDetails");
                            XmlDocument paymentResponseDetailsXml = this.xmlService.GetXmlDocument(xmlString);
                            batchDetail.NSUHost = this.xmlService.GetValue(paymentResponseDetailsXml, "/PaymentResponseDetails/Card[1]/AuthorizationDetails[1]/NSUHost");
                            batchDetail.CaptureDetailsAmount = this.xmlService.GetValue(paymentResponseDetailsXml, "/PaymentResponseDetails/Card[1]/CaptureDetails/Amount");
                            batchDetail.ApprovalCode = this.xmlService.GetValue(paymentResponseDetailsXml, "/PaymentResponseDetails/Card[1]/AuthorizationDetails/ApprovalCode");
                            batchDetail.AuthorizationDateTime = this.xmlService.GetValue(paymentResponseDetailsXml, "/PaymentResponseDetails/Card[1]/AuthorizationDetails/DateTime");
                            batchDetail.CVV2Response = this.xmlService.GetValue(paymentResponseDetailsXml, "/PaymentResponseDetails/Card[1]/AuthorizationDetails/CVV2Response");

                            // add detail to batch
                            batchDictionary[batchId].Details.Add(batchDetail);                            
                        }
                    }
                }
            }

            return batchDictionary.Values;
        }
    }
}
