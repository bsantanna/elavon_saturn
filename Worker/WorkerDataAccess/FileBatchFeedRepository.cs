﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;

namespace Saturn.Worker.Data.Binary
{
    public class FileBatchFeedRepository : AbstractFeedRepository<Batch>
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FileBatchFeedRepository(IWorkerContext workerContext) : base(workerContext) {
            log.Debug("Creating instance...");
        }

        protected override DateTime GetDateTime(Batch batch)
        {
            DateTime dateTime;
            if (batch.TrailerHttpResult != null)
            {
                dateTime = batch.TrailerHttpResult.EndTime;
            }
            else if (batch.HeaderHttpResult != null)
            {
                dateTime = batch.HeaderHttpResult.EndTime;
            }
            else
            {
                dateTime = TimeProvider.Current.UtcNow;
            }

            return dateTime;
        }
    }
}
