﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saturn.Worker.Data.Sql
{
    public class SqlScheduleRepository : AbstractSqlRepository, IScheduleRepository
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SqlScheduleRepository(IWorkerContext workerContext) : base(workerContext) {
            log.Debug("Creating instance...");
        }

        public Schedule GetSchedule()
        {
            Schedule schedule = null;

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_TASKER_SERVICEPARAMETER_SELECT", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("ServiceName", SqlDbType.NVarChar, "WSGate.Services." + workerContext.WorkerName()));

                connection.Open();

                using (SqlDataReader scheduleDataReader = command.ExecuteReader())
                {
                    if (scheduleDataReader.HasRows)
                    {
                        while (scheduleDataReader.Read())
                        {
                            schedule = new Schedule();
                            schedule.ServiceID = GetValue(scheduleDataReader, "ServiceID");
                            schedule.MinuteToRun = GetValue(scheduleDataReader, "MinuteToRun");
                            schedule.HourToRun = GetValue(scheduleDataReader, "HourToRun");
                            schedule.ForceExecute = "0".Equals(GetValue(scheduleDataReader, "ForceExecute"));
                            schedule.ForceStop = "0".Equals(GetValue(scheduleDataReader, "ForceStop"));
                            schedule.LastExecution = GetDateTimeValue(scheduleDataReader, "LastExecution");
                            schedule.LastExecutionOK = GetDateTimeValue(scheduleDataReader, "LastExecutionOK");
                            schedule.LastExecutionTimer = GetDateTimeValue(scheduleDataReader, "LastExecutionTimer");
                        }
                    }
                }
            }

            return schedule;
        }

        public void UpdateSchedule(Schedule schedule)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = workerContext.SqlConnectionUri();

                SqlCommand command = new SqlCommand("PR_TASKER_SERVICEPARAMETER_UPDATE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(GetParameter("ServiceID", SqlDbType.NVarChar, schedule.ServiceID));
                command.Parameters.Add(GetParameter("ServiceName", SqlDbType.NVarChar, "WSGate.Services." + workerContext.WorkerName()));
                command.Parameters.Add(GetParameter("LastExecution", SqlDbType.DateTimeOffset, (DateTimeOffset) schedule.LastExecution));
                command.Parameters.Add(GetParameter("LastExecutionOK", SqlDbType.DateTimeOffset, (DateTimeOffset) schedule.LastExecutionOK));
                command.Parameters.Add(GetParameter("LastExecutionTimer", SqlDbType.DateTimeOffset, (DateTimeOffset) schedule.LastExecutionTimer));
                command.Parameters.Add(GetParameter("ForceExecute", SqlDbType.NVarChar, schedule.ForceExecute));
                command.Parameters.Add(GetParameter("ForceStop", SqlDbType.NVarChar, schedule.ForceStop));

                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
