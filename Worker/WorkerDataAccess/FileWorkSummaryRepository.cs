﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Saturn.Worker.Data.Binary
{
    public class FileWorkSummaryRepository : AbstractFileRepository<WorkSummary>, IWorkSummaryRepository
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FileWorkSummaryRepository(IWorkerContext workerContext) : base(workerContext) {
            log.Debug("Creating instance...");
        }

        public ICollection<WorkSummary> GetPendingWork()
        {
            ICollection<WorkSummary> pendingWork = new HashSet<WorkSummary>();

            string path = GetWorkingDirectory();
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, typeof(WorkSummary).Name + "_*" + GetFileExtension(), SearchOption.AllDirectories);
                foreach (string file in files)
                {
                    byte[] bytes = File.ReadAllBytes(file);
                    WorkSummary workSummary = Deserialize(bytes);
                    if (workSummary.PendingBatches.Count > 0)
                    {
                        pendingWork.Add(workSummary);
                        File.Delete(file);
                    }
                }
            }

            return pendingWork;

        }

        protected override string GetFileExtension()
        {
            return ".bin";
        }

        protected override DateTime GetDateTime(WorkSummary workSummary)
        {
            return workSummary.StartTime;
        }

        public string Store(WorkSummary workSummary)
        {
            return StoreBytes(workSummary);
        }
    }
}
