﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Saturn.Worker.Data.Sql
{
    public abstract class AbstractSqlRepository
    {
        protected IWorkerContext workerContext;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AbstractSqlRepository(IWorkerContext workerContext)
        {
            log.Debug("Creating instance...");

            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;
        }

        protected SqlParameter GetParameter(string parameterName, SqlDbType parameterType, object value)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = "@" + parameterName;
            parameter.SqlDbType = parameterType;
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = value;
            return parameter;
        }

        protected string GetValue(SqlDataReader dataReader, string columKey)
        {
            string value = null;
            int columnIndex = dataReader.GetOrdinal(columKey);
            if (columnIndex > 0)
            {
                Object obj = dataReader.GetValue(columnIndex);
                value = obj.ToString();
            }
            return value;
        }

        protected DateTime GetDateTimeValue(SqlDataReader dataReader, string columnKey)
        {
            return Convert.ToDateTime(GetValue(dataReader, columnKey));
        }
    }
}
