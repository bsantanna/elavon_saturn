﻿using log4net;
using Saturn.Worker.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Saturn.Worker.Data.Binary
{
    public abstract class AbstractFileRepository<T>
    {
        protected readonly IWorkerContext workerContext;

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AbstractFileRepository(IWorkerContext workerContext)
        {
            log.Debug("Creating instance...");
            if (workerContext == null)
            {
                throw new ArgumentNullException("workerContext");
            }
            this.workerContext = workerContext;
        }

        public virtual string GetWorkingDirectory()
        {
            string workingDir = Path.Combine(this.workerContext.AppDataDirectory(), "Saturn_Worker", this.workerContext.WorkerName());

            if (!Directory.Exists(workingDir))
            {
                Directory.CreateDirectory(workingDir);
            }

            return workingDir;
        }

        protected T Deserialize(byte[] bytes)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(bytes);
            object obj = bf.Deserialize(ms);
            return (T)obj;
        }

        protected byte[] Serialize(T obj)
        {
            var stream = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(stream, obj);
            return stream.ToArray();
        }

        public string StoreBytes(T obj)
        {
            string path = CreatePath(obj);
            string directory = path.Substring(0, path.LastIndexOf("\\"));
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            byte[] bytes = Serialize(obj);
            File.WriteAllBytes(path, bytes);

            return path;
        }

        protected string CreatePath(T obj)
        {
            string workingDir = GetWorkingDirectory();
            DateTime dateTime = GetDateTime(obj);
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");

            return Path.Combine(workingDir, year, month, day, typeof(T).Name + "_" + obj.GetHashCode() + GetFileExtension());;
        }

        public ICollection<T> GetAllFromDate(DateTime dateTime)
        {
            ICollection<T> objSet = new HashSet<T>();
            string day = dateTime.ToString("dd");
            string month = dateTime.ToString("MM");
            string year = dateTime.ToString("yyyy");
            string path = Path.Combine(GetWorkingDirectory(), year, month, day);

            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, typeof(T).Name + "_*" + GetFileExtension(), SearchOption.TopDirectoryOnly);
                foreach (string file in files)
                {
                    byte[] bytes = File.ReadAllBytes(file);
                    objSet.Add(Deserialize(bytes));
                }
            }

            return objSet;
        }

        public ICollection<T> GetAll()
        {
            ICollection<T> objSet = new HashSet<T>();
            string path = GetWorkingDirectory();

            string[] files = Directory.GetFiles(path, typeof(T).Name + "_*" + GetFileExtension(), SearchOption.AllDirectories);
            foreach (string file in files)
            {
                byte[] bytes = File.ReadAllBytes(file);
                objSet.Add(Deserialize(bytes));
            }

            return objSet;
        }

        protected abstract string GetFileExtension();

        protected abstract DateTime GetDateTime(T obj);

    }
}
