﻿using log4net;
using Saturn.Core.API;
using Saturn.Worker.Domain;
using System;

namespace Saturn.Worker.Data.Binary
{
    public class FileBatchDetailFeedRepository : AbstractFeedRepository<BatchDetail>
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FileBatchDetailFeedRepository(IWorkerContext workerContext) : base(workerContext) {
            log.Debug("Creating instance...");
        }
        
        protected override DateTime GetDateTime(BatchDetail batchDetail)
        {
            if (batchDetail.CardData != null)
            {
                batchDetail.CardData = batchDetail.CardData.Substring(0, 4) + "...";
            }

            if (batchDetail.PINBlockDetails != null)
            {
                batchDetail.PINBlockDetails = null;
            }

            DateTime dateTime;
            if (batchDetail.DetailHttpResult != null)
            {
                dateTime = batchDetail.DetailHttpResult.EndTime;
            }
            else
            {
                dateTime = TimeProvider.Current.UtcNow;
            }
            return dateTime;
        }
    }
}
