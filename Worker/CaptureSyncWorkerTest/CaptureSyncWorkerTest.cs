﻿using System;
using Saturn.Worker.Domain.Test;
using Castle.MicroKernel.Registration;
using Saturn.Core.API;
using Saturn.Worker.Common;
using Saturn.Worker.Error;
using Saturn.Worker.Feed;
using Saturn.Worker.Logs;
using Saturn.Worker.Domain;
using NUnit.Framework;

namespace Saturn.Worker.Capture.Sync
{
    [TestFixture]
    public class CaptureSyncWorkerTest : AbstractWorkerTest
    {

        public CaptureSyncWorkerTest()
            : base(new IWindsorInstaller[] { 
                 new WorkerUnitTestInstaller(),
                 new ServiceInstaller(),
                 new CircuitBreakerInstaller(),
                 new WorkerCommonInstaller(),
                 new WorkerErrorHandlingInstaller(),
                 new WorkerLogsInstaller(),
                 new WorkerFeedInstaller(),
                 new CaptureSyncWorkerInstaller() }) { }

        [Test]
        public void TestDepedencyResolve()
        {
            var container = GetContainer();
            IWorker worker = GetWorker<CaptureSyncWorker>();
            Assert.IsNotNull(worker);
        }
    }
}
